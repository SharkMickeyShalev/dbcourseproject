﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Data;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (IsPostBack)
        {
            if (logPassword.Text.Equals("admin"))
            {
                Manager.log("Administration login requested (Password: " + logPassword.Text + ")");
                Session.Add("user", new global::User("999999999", "Administrator", EAuth.Administrator, new DateTime()));
                Manager.loggedUser = (User)Session["user"];

                
                Response.Redirect("/Dashboard_admin.aspx");
            }
            else lblError.Text = "Password incorrect!";
        }  
    }
  
}