﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;


namespace iGarden
{
    
    public class DB
    {
        private SqlConnection iGardenConnection;
        private SqlCommand cmd;

        public DB()
        {
            iGardenConnection = new SqlConnection("user id=maozako_igarden;" +
                                                  "password=q1w2e3r4;" +
                                                  "server=mssql4.1host.co.il;" +
                                                  "database=maozako_igarden_project; " +
                                                  "connection timeout=30");

        }
        public DataTable runProcWithResults(string procName, Dictionary<String, Object> valMap)
        {
            return runProcWithResults(procName, valMap, true);
            
            
        }
        public DataTable runProcWithResults(string procName, Dictionary<String, Object> valMap, bool writeToLog)
        {
            if(!writeToLog) Manager.writeToLog = false;
            
                Manager.log("\n\n\n");
                Manager.log("Received instruction to run procedure " + procName);
            
            DataTable toReturn = new DataTable();
            cmd= new SqlCommand();
            cmd.CommandText = procName;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = iGardenConnection;


            if (iGardenConnection.State != ConnectionState.Closed) {
                Manager.log("iGarden Connection is still open! Closing connection..");
                iGardenConnection.Close();
            }
                

            try
            {
                //Add values to the PROC Execution
                String queryString = "EXEC " + procName + " ";

                foreach (KeyValuePair<String, Object> entry in valMap)
                {
                    queryString += "@" + entry.Key + "='" + entry.Value+"',";
                    cmd.Parameters.AddWithValue("@" + entry.Key, entry.Value);
                    Manager.log("Adding parameter @" + entry.Key + "=" + entry.Value);
                }
                queryString = queryString.Substring(0, queryString.Length-1);

                Manager.log("Sending query:\n" + queryString);
                iGardenConnection.Open();
                
                Manager.log("Opened iGarden Connection");
                SqlDataAdapter dr = new SqlDataAdapter(cmd);
                Manager.log("Sending SQL Statement: " + cmd.CommandText+" with "+cmd.Parameters.Count+" parameters.");
                Manager.log("Returned #" + dr.Fill(toReturn)+" rows!!");

                String columns = "";
                foreach (DataColumn dc in toReturn.Columns)
                {
                    columns += dc.ColumnName + " ";
                }
                Manager.log("Returned Column List: \n" + columns);

                //For EXEC DML
                //myCommand.ExecuteNonQuery();
                Manager.log("Closing connection");
                    iGardenConnection.Close();


                    return toReturn;
                
            }
            catch (Exception e)
            {
                Manager.log("!!!Exception Occured!!!\n"+e.Message+"\n" + e.StackTrace);
               return new DataTable();
            }finally
            {
                Manager.writeToLog = true;
            }
        }
       
    }
}