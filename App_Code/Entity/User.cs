﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iGarden;

/// <summary>
/// Summary description for User
/// </summary>
public class User
{
    private String userID, fullName;
    private EAuth Auth;
    private DateTime dob;
    public User(String userID, String fullName, EAuth Auth, DateTime dob)
    {
        Manager.log("Creation of user requested (" + userID + " " + Auth);
        this.userID = userID;
        this.fullName = fullName;
        this.Auth = Auth;
        this.dob = dob;
        System.Web.HttpContext.Current.Session["userID"] = userID;
        System.Web.HttpContext.Current.Session["fullName"] = fullName;
        System.Web.HttpContext.Current.Session["Auth"] = Auth;
        System.Web.HttpContext.Current.Session["dob"] = dob;
        System.Web.HttpContext.Current.Session["user"] = this;


    }

    public String getUserID()
    {
        return userID;
    }

    public String getName()
    {
        return this.fullName;
    }

    public String getAuthType()
    {
        return this.Auth.ToString();
    }

    public EAuth getAuth()
    {
        return this.Auth;
    }
    
    
}