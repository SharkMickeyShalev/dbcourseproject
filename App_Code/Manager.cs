﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace iGarden
{
    public static class Manager
    {
        public static User loggedUser;
        public static bool writeToLog = true;
        //Reset the file
        public static System.IO.StreamWriter file = new System.IO.StreamWriter(getPath() + "log.txt", false);


        private static bool loaded = false;
        public static string siteURL = "http://localhost:2530/";
        //public static string siteURL = "http://www.radio-head.co.il/";
        private static Object lockThis = new Object();
        private static DB db;

        public static void update()
        {
            loggedUser = (User)System.Web.HttpContext.Current.Session["user"];
            lock (lockThis)
            {
                if (!loaded)
                {
                    loaded = true;
                    db = new DB();

                }
            }
        }
        public static System.Data.DataTable runProcWithResults(String procName)
        {
            return runProcWithResults(procName, new Dictionary<string, object>());
        }
        public static System.Data.DataTable runProcWithResults(String procName, Dictionary<String, Object> valMap)
        {
            update();
            return db.runProcWithResults(procName, valMap);
        }
        public static System.Data.DataTable runProcWithResults(String procName, Dictionary<String, Object> valMap, bool writeToLog)
        {
            update();
            return db.runProcWithResults(procName, valMap, writeToLog);
        }
        /* public static string execProc()
         {
             return db.runProcWithResults("", new Dictionary<string, string>());
         }
         */

        public static void log(String msg)
        {
            if (!writeToLog)
                return;
            file.Close();
            file = new System.IO.StreamWriter(getPath() + "log.txt", true);
            file.WriteLine(DateTime.Now.ToString("hh:mm:ss") + " - " + msg);
            file.Flush();
            file.Close();

        }

        public static GoogleGeoCodeResponse GEOCodeAddress(String Address)
        {
            Manager.log("Sending address " + Address);
            var address = String.Format("http://maps.google.com/maps/api/geocode/json?address={0}&sensor=false", Address.Replace(" ", "+"));
            Manager.log("Parsed address for google maps api: " + address);
            var result = new System.Net.WebClient().DownloadString(address);
            JavaScriptSerializer jss = new JavaScriptSerializer();
            return jss.Deserialize<GoogleGeoCodeResponse>(result);
        }
        public static void addNotification(String title, String message, int status)
        {
            if (loggedUser == null)
            {
                return;
                
            }
                
            Dictionary<String, Object> addMap = new Dictionary<string, object>();
            addMap.Add("userid", Manager.loggedUser.getUserID());
            addMap.Add("title", title);
            addMap.Add("text", message);
            addMap.Add("stat", status);

            Manager.runProcWithResults("add_notification", addMap, false);

            Manager.log("Added notification to user " + Manager.loggedUser.getUserID() + ": " + title + " \n" + message);

        }


        public static String getColorByFloat(float parsedRank)
        {
            string color = "gray";
            if (parsedRank > 9.5)
                color = "green";
            else            
            if (parsedRank > 7.5)
                color = "lightgreen";
            else if (parsedRank > 5)
                color = "orange";
            else if (parsedRank > 2.5)
                color = "firebrick";
            else if (parsedRank > 0) color = "red";
            return color;
        }
        private static readonly Random rand = new Random();

        public static Color GetRandomColour()
        {
            return Color.FromArgb(rand.Next(256), rand.Next(256), rand.Next(256));
        }
        public static string TimeAgo(DateTime dt)
        {
            TimeSpan span = DateTime.Now - dt;
            if (span.Days > 365)
            {
                int years = (span.Days / 365);
                if (span.Days % 365 != 0)
                    years += 1;
                return String.Format("about {0} {1} ago",
                years, years == 1 ? "year" : "years");
            }
            if (span.Days > 30)
            {
                int months = (span.Days / 30);
                if (span.Days % 31 != 0)
                    months += 1;
                return String.Format("about {0} {1} ago",
                months, months == 1 ? "month" : "months");
            }
            if (span.Days > 0)
                return String.Format("about {0} {1} ago",
                span.Days, span.Days == 1 ? "day" : "days");
            if (span.Hours > 0)
                return String.Format("about {0} {1} ago",
                span.Hours, span.Hours == 1 ? "hour" : "hours");
            if (span.Minutes > 0)
                return String.Format("about {0} {1} ago",
                span.Minutes, span.Minutes == 1 ? "minute" : "minutes");
            if (span.Seconds > 5)
                return String.Format("about {0} seconds ago", span.Seconds);
            if (span.Seconds <= 5)
                return "just now";
            return string.Empty;
        }
        public static String HexConverter(System.Drawing.Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }



        //get relative path of file
        private static String getPath()
        {
            String path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            path = path.Replace("\\","/");
            path = path.Substring(6);

            return path;
        }

    }



}