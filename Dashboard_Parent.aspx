﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Dashboard_Parent.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    Homepage
</asp:Content>
<asp:Content ContentPlaceHolderID="headerFirst" runat="server">
    Homepage
</asp:Content>
<asp:Content ContentPlaceHolderID="headerSecond" runat="server">
    Dashboard
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="head_include" Runat="Server">
   

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">


    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-2">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-primary pull-right"><%=DateTime.Today.ToString("dd/MM/yy") %></span>
                        <h5>Children</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">
                            <asp:Literal text="0" ID="totalKids" runat="server"></asp:Literal>
                        </h1>
                        <div class="stat-percent font-bold text-success">
                           
                            <i class="fa fa-child"></i></div>
                        <small>Total kids</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right">Annual</span>
                        <h5>Expenses</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">
                            $<asp:Literal text="0" ID="totalExpenses" runat="server"></asp:Literal>

                        </h1>
                        <div class="stat-percent font-bold text-info">
                            <asp:Literal ID="diffActivities" runat="server"></asp:Literal> Activities
                             <i class="fa fa-gamepad"></i></div>
                        <small>Total Expenses</small>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-primary pull-right">Today</span>
                        <h5>Reviews Overview</h5>
                    </div>
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-md-6">
                                <h1 class="no-margins">
                                    <asp:Literal text="0" ID="ltTotalParentReviews" runat="server"></asp:Literal>

                                </h1>
                                <div class="font-bold text-navy">
                                    <asp:Literal ID="ltReviewPer" runat="server"></asp:Literal>% <i class="fa fa-level-up"></i> <small>out of <asp:Literal ID="ltTotalReviews" runat="server"></asp:Literal></small></div>
                            </div>
                            <div class="col-md-6">
                                <h1 class="no-margins">
                                    <asp:Literal ID="ltAvgReviews" Text="0" runat="server"></asp:Literal>
                                </h1>
                                <div class="font-bold text-navy"><i class="fa fa-level-up"></i> <small>Your Average Reviews</small></div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <div class="col-lg-4">
     
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>$ Cost / Child</h5>

                        </div>
                        <div class="ibox-content">
                            <div class="text-center">
                                <canvas id="polarChart" height="250" width="791" style="width: 633px; height: 295px;"></canvas>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div>
                                        <span class="pull-right text-right">
                                        <small>Activities represented by <strong>total average</strong></small>
                                            <br>
                                            Total Activities: <asp:Literal ID="ltTotalActivities" runat="server"></asp:Literal>
                                        </span>
                            <h3 class="font-bold no-margins">
                                Reviews / Activities
                            </h3>
                            <h5 style="color: rgba(220,220,220,0.5);"><b>Total Average Reviews</b><br />
                            <span style="color: rgba(26,179,148,0.5);">Your Average Reviews</span></h5>
                     
                        </div>

                        <div class="m-t-sm">

                            <div class="row">
                                <div class="col-md-8">
                                    <div>
                                        <canvas id="lineChart" height="156" width="412" style="width: 412px; height: 156px;"></canvas>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <ul class="stat-list m-t-lg">
                                        <li>
                                            <h2 class="no-margins">
                                                <asp:Literal ID="ltChartMyReviewCount" runat="server"></asp:Literal>
                                            </h2>
                                            <small>Your Reviews</small>
                                            <div class="progress progress-mini">
                                                <div class="progress-bar" style="width: 48%;"></div>
                                            </div>
                                        </li>
                                        <li>
                                            <h2 class="no-margins ">
                                                <asp:Literal ID="ltChartTotalReviewCount" runat="server"></asp:Literal>
                                            </h2>
                                            <small>Total Reviews</small>
                                            <div class="progress progress-mini">
                                                <div class="progress-bar" style="width: 60%;"></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                        <div class="m-t-md">
                            <small class="pull-right">
                                <i class="fa fa-clock-o"> </i>
                                Updated <%=DateTime.Today.ToString("dd/MM/yy") %>
                            </small>
                            <small>
                            </small>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-warning pull-right">Top 5</span>
                        <h5>Activities</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-xs-1">
                                <h4>#</h4>
                            </div>
                            <div class="col-xs-3">
                                <h4>Name</h4>
                            </div>

                            <div class="col-xs-4">
                                <h4>Average</h4>
                            </div>
                            <div class="col-xs-2">
                                <h4>Price</h4>
                            </div>
                        </div>
                        <asp:Literal ID="actStatsTop5" runat="server"></asp:Literal>
                      
                    </div>
                   
                </div>
                  <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-warning pull-right">Top 5</span>
                        <h5>Kindergardens</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-xs-1">
                                <h4>#</h4>
                            </div>
                            <div class="col-xs-3">
                                <h4>Name</h4>
                            </div>

                            <div class="col-xs-4">
                                <h4>Average</h4>
                            </div>
                            <div class="col-xs-2">
                                <h4>Price</h4>
                            </div>
                        </div>
                        <asp:Literal ID="Top5KGList" runat="server"></asp:Literal>
                      
                    </div>
                   
                </div>
            </div>

        </div>



        </div>

</asp:Content>
<asp:Content ContentPlaceHolderID="jsLoad" runat="server">
    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="js/plugins/flot/jquery.flot.pie.js"></script>

    <!-- Peity -->
    <script src="js/plugins/peity/jquery.peity.min.js"></script>
    <script src="js/demo/peity-demo.js"></script>


    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- GITTER -->
    <script src="js/plugins/gritter/jquery.gritter.min.js"></script>

    <!-- Sparkline -->
    <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="js/demo/sparkline-demo.js"></script>

    <!-- ChartJS-->
    <script src="js/plugins/chartJs/Chart.min.js"></script>
 

    <!-- Toastr -->
    <script src="js/plugins/toastr/toastr.min.js"></script>
    <script>
        $(document).ready(function() {
            var polarData = [
  <%=getPolarChart()%>
            ];

            var polarOptions = {
                scaleShowLabelBackdrop: true,
                scaleBackdropColor: "rgba(255,255,255,0.75)",
                scaleBeginAtZero: true,
                scaleBackdropPaddingY: 1,
                scaleBackdropPaddingX: 1,
                scaleShowLine: true,
                segmentShowStroke: true,
                segmentStrokeColor: "#fff",
                segmentStrokeWidth: 2,
                animationSteps: 150,
                animationEasing: "easeOutBounce",
                animateRotate: true,
                animateScale: true,
                responsive: true,
                width: 100,
                height: 50

            };

            var ctx = document.getElementById("polarChart").getContext("2d");
            var myNewChart = new Chart(ctx).PolarArea(polarData, polarOptions);


  
            var lineData = {
                labels: [<%="" + getOpArray(1)%>],
                datasets: [
                    {
                        label: "Example dataset",
                        fillColor: "rgba(220,220,220,0.5)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: <%=""+ getOpArray(2)%>
                    },
                    {
                        label: "Example dataset",
                        fillColor: "rgba(26,179,148,0.5)",
                        strokeColor: "rgba(26,179,148,0.7)",
                        pointColor: "rgba(26,179,148,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(26,179,148,1)",
                        data: <%=""+ getOpArray(3)%>
                    }
                ]
            };

            var lineOptions = {
                scaleShowGridLines: true,
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleGridLineWidth: 1,
                bezierCurve: true,
                bezierCurveTension: 0.4,
                pointDot: true,
                pointDotRadius: 4,
                pointDotStrokeWidth: 1,
                pointHitDetectionRadius: 20,
                datasetStroke: true,
                datasetStrokeWidth: 2,
                datasetFill: true,
                responsive: true,
            };


            var ctx = document.getElementById("lineChart").getContext("2d");
            var myNewChart = new Chart(ctx).Line(lineData, lineOptions);

        });
    </script>
</asp:Content>