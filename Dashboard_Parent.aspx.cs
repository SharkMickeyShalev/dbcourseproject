﻿using System;
using iGarden;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;

public partial class _Default : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        Manager.update();
        try
        {
            
            //titleName.Text = Manager.loggedUser.getName();
            Dictionary<String, Object> dict = new Dictionary<string, object>();
            dict.Add("ParentID", Manager.loggedUser.getUserID());
            
            DataTable dt = Manager.runProcWithResults("dashboard_parent_general", dict);


            ltTotalActivities.Text = dt.Rows[0]["TotalActivities"].ToString();
            string sqlTotalKids = dt.Rows[0]["totalKids"].ToString();
            Manager.log("Total Kids: " + sqlTotalKids);
            int totalKGPrice = int.Parse(dt.Rows[0]["totalKGPrice"].ToString());
            dt = Manager.runProcWithResults("dashboard_parent_activities", dict);
            int sqlTotalExpenses = int.Parse(dt.Rows[0]["totalExpenses"].ToString());
            int numActivities = int.Parse(dt.Rows[0]["numActivities"].ToString());
            
            diffActivities.Text = numActivities.ToString();
            totalKids.Text = sqlTotalKids;
            totalExpenses.Text = (sqlTotalExpenses+totalKGPrice).ToString();

            string Top5ActText = "";
            using (DataTable tmp = Manager.runProcWithResults("get_opinions_act_list"))
            {
                for (int i = 0; i < Math.Min(5, tmp.Rows.Count); i++)
                {
                    Top5ActText += @"
                       <div class='row'>
                             <div class='col-xs-1'>
                                " + (i + 1) + @"
                            </div>
                            <div class='col-xs-3'>
                                " + tmp.Rows[i]["name"] + @"
                            </div>

                            <div class='col-xs-4' style='color: "+Manager.getColorByFloat(float.Parse(tmp.Rows[i]["avg"].ToString()) )+@";'>
                                " + tmp.Rows[i]["avg"] + @"
                            </div>
                            <div class='col-xs-2'>
                                $" + tmp.Rows[i]["cost"] + @"
                            </div>
                        </div>
";
                }
                actStatsTop5.Text = Top5ActText;
            }
                

            String Top5KGListText = "";
            using (DataTable tmp = Manager.runProcWithResults("get_opinions_kg_list"))
            {
                for (int i = 0; i < Math.Min(5, tmp.Rows.Count); i++)
                {
                    Top5KGListText += @"
                       <div class='row'>
                             <div class='col-xs-1'>
                                " + (i + 1) + @"
                            </div>
                            <div class='col-xs-3'>
                                " + tmp.Rows[i]["name"] + @"
                            </div>

                            <div class='col-xs-4' style='color: " + Manager.getColorByFloat(float.Parse(tmp.Rows[i]["avg"].ToString())) + @";'>
                                " + tmp.Rows[i]["avg"] + @"
                            </div>
                            <div class='col-xs-2'>
                                $" + tmp.Rows[i]["price"] + @"
                            </div>
                        </div>
";
                }



                Top5KGList.Text = Top5KGListText;

            }

            using(DataTable tmp = Manager.runProcWithResults("dashboard_parent_opinions", dict))
            {
                int totalReviews = int.Parse(tmp.Rows[0]["TotalReviews"].ToString());
                int totalParentReviews = int.Parse(tmp.Rows[0]["TotalParentReviews"].ToString());
                double reviewPer = Math.Round(((double)totalParentReviews / (double)totalReviews) *100);
                ltChartMyReviewCount.Text = tmp.Rows[0]["TotalParentReviews"].ToString();
                ltChartTotalReviewCount.Text = totalReviews.ToString();
                
                float avgReviews = float.Parse(tmp.Rows[0]["Average"].ToString());
                float maxReview= float.Parse(tmp.Rows[0]["Max"].ToString());
                float totalAverage = float.Parse(tmp.Rows[0]["totalAverage"].ToString());
                ltAvgReviews.Text = avgReviews.ToString();
                ltTotalReviews.Text = totalReviews.ToString();
                ltTotalParentReviews.Text = totalParentReviews.ToString();
                ltReviewPer.Text = reviewPer.ToString();
            }

        }
        catch (Exception ex)
        {
            Manager.log("Found Exception!: \n" + ex);
        }
    }
    protected String getOpArray(int choice)
    {
        String varNames = "";
        String toReturn = "[";
        String toReturn2 = "[";
        Dictionary<String, Object> getMap = new Dictionary<string, object>();
        getMap.Add("ParentID", Manager.loggedUser.getUserID());
        foreach(DataRow row in Manager.runProcWithResults("dashboard_parent_opinion_statistics_activities", getMap).Rows)
        {
                varNames += "\""+ row["name"]+ "\",";
                toReturn += row["ParentAvg"] + ",";
                toReturn2 += row["myGrade"]+",";
        }
        varNames = varNames.Substring(0, varNames.Length-1);
        toReturn = toReturn.Substring(0, toReturn.Length - 1) + "]";
        toReturn2 = toReturn2.Substring(0, toReturn2.Length - 1) + "]";
   
        switch (choice)
        {
            case 1:
                return varNames;
            case 2:
                return toReturn;
            default:
                return toReturn2;
        }
     

    }

    protected string getColor()
    {
        Color myColor =  Manager.GetRandomColour();
        string hex = myColor.R.ToString("X2") + myColor.G.ToString("X2") + myColor.B.ToString("X2");
        return hex;
    }

    protected string getPolarChart()
    {

        string toReturn = "";
        Dictionary<String, Object> map = new Dictionary<string, object>();
        map.Add("ParentID", Manager.loggedUser.getUserID());
        using(DataTable dt = Manager.runProcWithResults("dashboard_parent_kids_cost", map))
        {
            foreach (DataRow row in dt.Rows)
            {
                toReturn += @"
                    {
                        value: " + row["cost"]+@",
                    color: '#" + getColor() + @" ',
                    highlight: '#1ab394',
                     label: '"+row["fullname"]+@"'
                    },
    ";
            }
            if(toReturn.Length>0)
            toReturn = toReturn.Substring(0, toReturn.Length - 1);
        }

        if (toReturn.Length == 0)
        {
            toReturn = @"
              {
                        value: 1,
                    color: '#" + getColor() + @" ',
                    highlight: '#1ab394',
                     label: 'No Kids'
                    }
";
        }

        return toReturn;
    }
}