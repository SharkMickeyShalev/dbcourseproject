﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Dashboard_admin.aspx.cs" Inherits="Dashboard_admin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head_include" Runat="Server">
      <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="headerFirst" Runat="Server">
    Homepage
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="headerSecond" Runat="Server">
    Dashboard
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="wrapper wrapper-content">
        <div class="row">
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            
                            <div class="ibox-title">
                                <span class="label label-success pull-right"><%=DateTime.Today.ToShortDateString() %></span>
                                <h5>Income</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">$<%=totalProfit %></h1>
                                <div class="stat-percent font-bold text-success"><i class="fa fa-bolt"></i></div>
                                <small>Total Income</small>
                                
                                
                                <br /><br /><small>Kindergardens Income: $<%=kinProfit %></small><br />
<small>Activities Income: $<%=actProfit %></small>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right">Registered Kids</span>
                                <h5>Kindergardens Proportion</h5>
                            </div>
                              <div class="ibox-content">
                            <div>
                                <canvas id="doughnutChart" height="150"></canvas>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Registered to iGarden+</span>
                                <h5>Users Diversity</h5>
                            </div>
                         <div class="ibox-content">
                         <div class="flot-chart">
                             <div class="flot-chart-pie-content" id="flot-pie-chart" style="padding: 0px; position: relative;">
<canvas class="flot-base" width="179" height="179" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 200px; height: 200px;">
</canvas>
<canvas class="flot-overlay" width="179" height="179" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 200px; height: 200px;"></canvas>
<div class="legend">
<div style="position: relative; width: 54px; height: 58px; top: 5px; right: 5px; background-color: rgb(255, 255, 255); opacity: 0.85;">

</div><table style="position:absolute;top:5px;right:5px;;font-size:smaller;color:#545454">
                                                                                                                                                                                                                         <tbody><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px">
                                                                                                                                                                                                                             <div style="width:4px;height:0;border:5px solid #d3d3d3;overflow:hidden"></div></div></td>
                                                                                                                                                                                                                             <td class="legendLabel">Sales 1</td></tr><tr><td class="legendColorBox">
                                                                                                                                                                                                                                 <div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid #bababa;overflow:hidden"></div></div></td><td class="legendLabel">Sales 2</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid #79d2c0;overflow:hidden"></div></div></td><td class="legendLabel">Sales 3</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid #1ab394;overflow:hidden"></div></div></td><td class="legendLabel">Sales 4</td></tr></tbody></table></div></div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-danger pull-right">Top 1</span>
                                <h5>Top Activity</h5>
                            </div>
                         <%=topActivity %>
                        </div>
            </div>
        </div>
        <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Activity Overview</h5>
                                <div class="pull-right">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-xs btn-white active">Today</button>
                                        <button type="button" class="btn btn-xs btn-white">Monthly</button>
                                        <button type="button" class="btn btn-xs btn-white">Annual</button>
                                    </div>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                <div class="col-lg-9">
                                     <div>
                                <canvas id="radarChart"></canvas>
                            </div>
                                </div>
                                <div class="col-lg-3">
                                    <ul class="stat-list">
                                       <%=stats %>
                                        </ul>
                                    </div>
                                </div>
                                </div>

                            </div>
                        </div>
                    </div>


                <div class="row">
                    <div class="col-lg-4">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Messages</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content ibox-heading">
                                <h3><i class="fa fa-envelope-o"></i> Latest Activities</h3>
                                <small><i class="fa fa-tim"></i> There are <%=activities%> total activities registered..</small>
                            </div>
                            <div class="ibox-content">
                                <div class="feed-activity-list">

                                    <%=latestactivities %>


                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-8">

                            <div class="col-lg-9">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Activity View</h5>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                            <a class="close-link">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="ibox-content">
                                        <table class="table table-hover no-margins">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Registered</th>
                                                <th>Cost</th>
                                                <th>Profit</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                           <%=activitieslist %>
                                            </tbody>
                                        </table>
                                    </div>
                              
                            </div>
                     
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>iGarden WorldWide</h5>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                            <a class="close-link">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="ibox-content">

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <table class="table table-hover margin bottom">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 1%" class="text-center">No.</th>
                                                        <th>Location</th>
                                                        <th class="text-center">Date Implemented</th>
                                                        <th class="text-center">Amount</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td class="text-center">1</td>
                                                        <td> Israel
                                                            </td>
                                                        <td class="text-center small">12/17/2017</td>
                                                        <td class="text-center"><span class="label label-primary"><%=kgAmount %></span></td>

                                                    </tr>
                                                
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-lg-6">
                                               <div id="world-map" style="height: 300px;">
  
</div>
                                            </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
                </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="jsLoad" Runat="Server">
    
    <!-- Mainly scripts -->
    <script src="/js/jquery-2.1.1.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="/js/plugins/flot/jquery.flot.js"></script>
    <script src="/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="/js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="/js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="/js/plugins/flot/jquery.flot.time.js"></script>

    <!-- Peity -->
    <script src="/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="/js/demo/peity-demo.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/js/inspinia.js"></script>
    <script src="/js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- Jvectormap -->
    <script src="/js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

    <!-- EayPIE -->
    <script src="/js/plugins/easypiechart/jquery.easypiechart.js"></script>

    <!-- Sparkline -->
    <script src="/js/plugins/sparkline/jquery.sparkline.min.js"></script>
      <script src="js/plugins/chartJs/Chart.min.js"></script>


    <script>
        $(document).ready(function () {
        
         

            var mapData = {
                "IL": <%=kgAmount%>,
                
            };

            $('#world-map').vectorMap({
                map: 'world_mill_en',
                backgroundColor: "transparent",
                regionStyle: {
                    initial: {
                        fill: '#e4e4e4',
                        "fill-opacity": 0.9,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 0
                    }
                },

                series: {
                    regions: [{
                        values: mapData,
                        scale: ["#1ab394", "#22d6b1"],
                        normalizeFunction: 'polynomial'
                    }]
                },
            });

            //Flot Pie Chart
            $(function () {

                var data = [<%=pieLabels%>];

                var plotObj = $.plot($("#flot-pie-chart"), data, {
                    series: {
                        pie: {
                            show: true
                        }
                    },
                    grid: {
                        hoverable: true
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
                        shifts: {
                            x: 20,
                            y: 0
                        },
                        defaultTheme: false
                    }
                });

            });


            var doughnutData = [
                <%=doughnutData%>
               
            ];

            var doughnutOptions = {
                segmentShowStroke: true,
                segmentStrokeColor: "#fff",
                segmentStrokeWidth: 2,
                percentageInnerCutout: 45, // This is 0 for Pie charts
                animationSteps: 100,
                animationEasing: "easeOutBounce",
                animateRotate: true,
                animateScale: false,
                responsive: true,
            };


            var ctx = document.getElementById("doughnutChart").getContext("2d");
            var myNewChart = new Chart(ctx).Doughnut(doughnutData, doughnutOptions);

            var radarData = {
                labels: [<%=radarLabels%>],
                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(220,220,220,0.2)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [<%=radardata1%>]
                    },
                    {
                        label: "My Second dataset",
                        fillColor: "rgba(26,179,148,0.2)",
                        strokeColor: "rgba(26,179,148,1)",
                        pointColor: "rgba(26,179,148,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(151,187,205,1)",
                        data: [<%=radardata2%>]
                    }
                ]
            };

            var radarOptions = {
                scaleShowLine: true,
                angleShowLineOut: true,
                scaleShowLabels: false,
                scaleBeginAtZero: true,
                angleLineColor: "rgba(0,0,0,.1)",
                angleLineWidth: 1,
                pointLabelFontFamily: "'Arial'",
                pointLabelFontStyle: "normal",
                pointLabelFontSize: 10,
                pointLabelFontColor: "#666",
                pointDot: true,
                pointDotRadius: 3,
                pointDotStrokeWidth: 1,
                pointHitDetectionRadius: 20,
                datasetStroke: true,
                datasetStrokeWidth: 2,
                datasetFill: true,
                responsive: true,
            }

            var ctx = document.getElementById("radarChart").getContext("2d");
            var myNewChart = new Chart(ctx).Radar(radarData, radarOptions);


        });

    </script>
</asp:Content>

