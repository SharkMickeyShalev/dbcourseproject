﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Data;

public partial class Dashboard_admin : System.Web.UI.Page
{
    protected int activities = 2;
    protected string latestactivities = "";
    protected string pieLabels = "";
    protected int kgAmount = 0;
    protected int totalProfit = 0;
    protected int kinProfit = 0;
    protected int actProfit = 0;
    protected string doughnutData = "";
    protected string topActivity = "";
    protected string radarLabels = "", radardata1 = "", radardata2 = "", stats="", activitieslist="";
    
    int i = 0;
    protected void Page_Load(object sender, EventArgs e)
    {

        using (DataTable dt = Manager.runProcWithResults("dashboard_admin_top1act"))
        {
            topActivity +=
                @" <div style='position: relative; width: 100%; background-image: url(/images/activities/" + dt.Rows[0][0] + @".jpg); background-repeat: no-repeat;'>

      <img src='images/activities/" + dt.Rows[0][0]+@".jpg' style='max-height: 230px; visibility: hidden;' />
      
      <h2 style='  position: absolute; 
   top: 0px; 
   text-align: center;

   width: 100%;'><span style='color: white; 
   font: bold 12px/0px Helvetica, Sans-Serif; 
   letter-spacing: -1px;  
   color: black;

   background: rgb(0, 0, 0); /* fallback color */
   background: rgba(0, 0, 0, 0.0);
   padding: 10px;'><span style='font-size: 20px;'>"+dt.Rows[0][1]+@"</span>
</h2>
    
      <h2 style='  position: absolute; 
   top: 10px; 
left: ;
   text-align: left;

   width: 100%;'><span style='color: white; 
   font: bold 12px/32px Helvetica, Sans-Serif; 
   letter-spacing: -1px;  
   color: black;

   background: rgb(0, 0, 0); /* fallback color */
   background: rgba(0, 0, 0, 0.0);
   padding: 0px;'>Total Income: $" + dt.Rows[0][3] + @"<br />
Participants: " + dt.Rows[0][2] + @"</span></h2>


</div>";




          
        }

        using (DataTable dt = Manager.runProcWithResults("dashboard_admin_users_statistics"))
        {
            foreach(DataColumn col in dt.Columns)
            {
        
                if (col.ColumnName.Equals("Total"))
                    break;
                double test = (double.Parse(dt.Rows[0][col.ColumnName].ToString()) / double.Parse(dt.Rows[0]["Total"].ToString())) * 100;
                pieLabels += @"{
label: '"+col.ColumnName+@"',
                    data: "+ test+ @",
            color: '"+Manager.HexConverter(Manager.GetRandomColour())+@"',
                },
        ";
            }
        }
        using (DataTable dt = Manager.runProcWithResults("dashboard_admin_totalprofit"))
        {
            totalProfit = int.Parse(dt.Rows[0][0].ToString()) + int.Parse(dt.Rows[0][1].ToString());
            kinProfit = int.Parse(dt.Rows[0][0].ToString());
            actProfit = int.Parse(dt.Rows[0][1].ToString());
        }
           
        /*using (DataTable dt = Manager.runProcWithResults("dashbaord_admin"))
        {

        }
        */

        using (DataTable dt = Manager.runProcWithResults("dashboard_admin_kindergardens")){

            foreach (DataRow row in dt.Rows)
            {
                doughnutData+= @"{
                value: "+row[1]+@",
                    color: '"+Manager.HexConverter(Manager.GetRandomColour())+@"',
                    highlight: '#1ab394',
                    label: '"+row[0]+@"'
                },
";
            }
            
        }

        using (DataTable dt = Manager.runProcWithResults("dashboard_income_of_activities"))
        {
            foreach(DataRow row in dt.Rows)
            {
                radarLabels += "\""+row["name"]+"\",";
                radardata1 += "" + row["TotalIncome"] + ",";
                radardata2 += "" + row["cost"]+",";

                activitieslist += @"
 <tr>
                                                <td><small>"+row["name"]+@"</small></td>
                                                <td><i class='fa fa-clock - o'></i>"+row["TotalKids"]+@"</td>
                                                  <td> $"+row["cost"]+@"</ td >
  
                                                  <td class='text-navy'> <i class='fa fa-level-up'></i> $"+row["TotalIncome"]+@" </td>
                                            </tr>
";

            }

            radarLabels = radarLabels.Substring(0, radarLabels.Length - 1);
            radardata1 = radardata1.Substring(0, radardata1.Length - 1);
            radardata2 = radardata2.Substring(0, radardata2.Length - 1);
        }

        using (DataTable dt = Manager.runProcWithResults("get_latest_notifications"))
        {
            activities = dt.Rows.Count;
            foreach (DataRow row in dt.Rows)
            {
                latestactivities += @"<div class='feed-element'>
                                        <div>
                                            <small class='pull-right text-navy'>" + Manager.TimeAgo((DateTime)row["nDate"]) + "<br />by " +row["surName"] + " " + row["firstName"] +  @" </small>
                                            <strong>" + row["strTitle"] + @"</strong>
                                            <div>" + row["msg"] + @"</div>
                                            <small class='text-muted'>" + row["nDate"]+@"</small>
                                        </div>
                                    </div>";
                if ((i++) == 5)
                    break;
            }
        }

        using (DataTable dt = Manager.runProcWithResults("dashboard_admin_statistics"))
        {
            kgAmount = int.Parse(dt.Rows[0][8].ToString());
            stats += @"<li>
                                            <h2 class='no-margins'>"+dt.Rows[0][1]+@" out of "+dt.Rows[0][2]+@"</h2>
                                            <small>Active Activities</small>
                                            <div class='stat-percent'>"+float.Parse(dt.Rows[0][1].ToString())/float.Parse(dt.Rows[0][2].ToString())*100+ @"% <i class='fa fa-level-up text-navy'></i></div>
                                            <div class='progress progress-mini'>
                                                <div style = 'width: " + float.Parse(dt.Rows[0][1].ToString()) / float.Parse(dt.Rows[0][2].ToString())*100 + @"%;' class='progress-bar'></div>
                                            </div>
                                        </li>
                                       <li>
                                            <h2 class='no-margins'>" + dt.Rows[0][3] + @" out of " + dt.Rows[0][4] + @"</h2>
                                            <small>Children signed to activities</small>
                                            <div class='stat-percent'>" + float.Parse(dt.Rows[0][3].ToString()) / float.Parse(dt.Rows[0][4].ToString()) * 100 + @"% <i class='fa fa-level-up text-navy'></i></div>
                                            <div class='progress progress-mini'>
                                                <div style = 'width: " + float.Parse(dt.Rows[0][3].ToString()) / float.Parse(dt.Rows[0][4].ToString()) * 100 + @"%;' class='progress-bar'></div>
                                            </div>
                                        </li>        <li>
                                            <h2 class='no-margins'>" + dt.Rows[0][6] + @" out of " + dt.Rows[0][5] + @"</h2>
                                            <small>Opinions are <span style='color: green;'>above</span> the grade 5</small>
                                            <div class='stat-percent'>" + float.Parse(dt.Rows[0][6].ToString()) / float.Parse(dt.Rows[0][5].ToString()) * 100 + @"% <i class='fa fa-level-up text-navy'></i></div>
                                            <div class='progress progress-mini'>
                                                <div style = 'width: " + float.Parse(dt.Rows[0][6].ToString()) / float.Parse(dt.Rows[0][5].ToString()) * 100 + @"%;' class='progress-bar'></div>
                                            </div>
                                        </li>
                                               <li>
                                            <h2 class='no-margins'>" + dt.Rows[0][7] + @" out of " + dt.Rows[0][5] + @"</h2>
                                            <small>Opinions are <span style='color: red;'>below</span> the grade 5</small>
                                            <div class='stat-percent'>" + float.Parse(dt.Rows[0][7].ToString()) / float.Parse(dt.Rows[0][5].ToString()) * 100 + @"% <i class='fa fa-level-up text-navy'></i></div>
                                            <div class='progress progress-mini' >
                                                <div style = 'background-color: red; width: " + float.Parse(dt.Rows[0][7].ToString()) / float.Parse(dt.Rows[0][5].ToString()) * 100 + @"%;' class='progress-bar'></div>
                                            </div>
                                        </li>
                                       

";
        }

    }
}