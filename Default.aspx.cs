﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] == null)
            Response.Redirect("/login.aspx");
        switch (((User)Session["user"]).getAuth())
        {
            case EAuth.Administrator:
                Response.Redirect("/Dashboard_admin.aspx");
                return;
            default:
                Response.Redirect("/Dashboard_parent.aspx");
                break;
        }
    }
}