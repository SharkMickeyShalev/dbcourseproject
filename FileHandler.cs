﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;


public class FileHandler
{
    private static FileHandler instance;
    private Dictionary<String, String> prm;
    
    //private constructor
    public FileHandler()
    {
        prm = new Dictionary<string, string>();
    }


    public FileHandler getInstance()
    {
        if (instance == null)
        {
            instance = new FileHandler();
        }
        return instance;
    }

    //get hashmap of params to read fro it - required to be umdoifiable
    public Dictionary<String, String> getParams()
    {
        return prm;
    }

    //write line to specific file - gets file name and text as parameters
    public void writeToFile(String fileName, ArrayList lines)
    {
        fileName = "temp.txt";

        //create writer object using relative path of file
        try
        {
            String path = Directory.GetCurrentDirectory().ToString();
            path = path.Replace('\\', '/');
            StreamWriter sw = File.AppendText(path + fileName);
            //write lines to file
            foreach (String line in lines)
            {
                sw.WriteLine(line);
            }




            //sw.WriteLine("null"); sw.Flush();
            //Console.WriteLine(path);

        }
        catch (IOException e)
        {
            //DO SOMETHING
        }



        Console.ReadKey();
    }



    //read lines of given file
    public void readFile(String fileName)
    {
        fileName = "temp.txt";

        //create writer object using relative path of file
        String path = Directory.GetCurrentDirectory().ToString();
        path = path.Replace('\\', '/');
        try
        {
            String line = null;
            StreamReader read = new StreamReader(path + fileName);
            while ((line = read.ReadLine()) != null)
            {
                //do something with this line
            }
        } catch (IOException e)
        {
            //DO SOMETHING
        }
    }
    

}
