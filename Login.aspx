﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>iGarden | Login</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown igardenlogo">
        <div>
            <div>
                
                <h1 class="logo-name" style="padding-left: 20px; padding-bottom: 10px;font-size: 80px; ">iG+</h1>

            </div>
            <h3>Welcome to iGarden+</h3>
            <p>iGarden is a Haifa Distrubuted Software for Garden Managemant Dashboard & Courses Registration
                <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
            </p>
            <p  style="color: red; font-size: 10px;">
                <asp:Literal ID="lblError" runat="server"></asp:Literal>
            </p>
            <form class="m-t" role="form" action="Login.aspx" runat="server">

                <div class="form-group">
                    <!--<input type="email" class="form-control" placeholder="Username" required="">-->
                    <asp:TextBox  ID="logID" runat="server" CssClass="form-control" placeholder="User ID"  ></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RegularExpressionValidator1" runat="server"
             ErrorMessage="Invalid ID (9 Digits)" ControlToValidate="logID"
                         Display="Dynamic"
             SetFocusOnError="True"
             ValidationExpression="^\d{9}$">
</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RequiredFieldValidator1" runat="server"
             ErrorMessage="Invalid ID (9 Digits)" ControlToValidate="logID"
                        Display="Dynamic"
             SetFocusOnError="True"
             ValidationExpression="^\d{9}$">
</asp:RegularExpressionValidator>
                </div>
                <div class="form-group">
                     <asp:TextBox  ID="logPassword" TextMode="Password" type="password" runat="server" CssClass="form-control" placeholder="Password" ></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="logPassword" runat="server"
                        SetFocusOnError="true" ErrorMessage="Password field cannot be empty"></asp:RequiredFieldValidator>
                </div>
                
                <asp:Button ID="Button1" CssClass="btn btn-primary block full-width m-b" runat="server" Text="Log In iGarden+" />
                

                <a href="callto:1-800-IGARDEN"><small>Forgot password? Call 1-800-IGARDEN</small></a>
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="/Register.aspx">Create an account</a>
                <a class="btn btn-sm btn-white btn-block" style="background-color: darkred; color: white;" href="/Admin.aspx">Administrator LogIn</a>
            </form>
            
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>

</html>

