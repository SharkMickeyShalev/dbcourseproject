﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Data;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (IsPostBack)
        {
            /**
             * Skip LOGIN
             **/
              Manager.log("Received log in request");
            Manager.log("Details: (" + logID.Text + " " + logPassword.Text + ")");
            if(logID.Text.Length==0 || logPassword.Text.Length == 0)
            {
                lblError.Text = "Please fill in all the details required";
                return;
            }
            //attempt to log in
            Dictionary<String, Object> toSend = new Dictionary<string, object>();
            toSend.Add("strUsername", logID.Text);
            toSend.Add("strPassword", logPassword.Text);
            Manager.log("Sending login request to procedure getLoginAuth with details ("+logID.Text+" "+logPassword.Text+")");
            using (DataTable dt = Manager.runProcWithResults("getLoginAuth", toSend))
            {
                Manager.log("Retreived datatable - " + dt.Rows.Count);
                if (dt == null || dt.Rows.Count == 0)
                {
                    Manager.log("Login failed due to DataTable returned contains 0 rows!");
                    lblError.Text = "Couldn't log in.";
                }else
                {
                    
                    User u = new User(dt.Rows[0]["ID"].ToString(), dt.Rows[0]["FullName"].ToString(), (EAuth)dt.Rows[0]["Auth"], Convert.ToDateTime(dt.Rows[0]["DOB"]));
                    //Manager.loggedUser = new User(dt.Rows[0]["ID"].ToString(), dt.Rows[0]["FullName"].ToString(), (EAuth)dt.Rows[0]["Auth"], Convert.ToDateTime(dt.Rows[0]["DOB"]));
                    Session.Add("user", u);

                            Response.Redirect("/Dashboard_parent.aspx");

                    
                   
                }

            }
        }
    }
  
}