﻿using System;
using iGarden;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;

public partial class MasterPage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
    


            if (Session["user"]==null || Manager.loggedUser==null)
            {
            Manager.log("Referring to login page");
                Response.Redirect("/Login.aspx");
            }

       
        Manager.loggedUser = (User)Session["user"];

        /**
         * User is logged in
         */

        updateNotifications();


        ltloggedName.Text = Manager.loggedUser.getName();
        headerTitle.Text = ((LiteralControl)head.Controls[0]).Text;
            lblUsername.Text = Manager.loggedUser.getName();
        lblUserAuthType.Text = Manager.loggedUser.getAuthType();
        
        imgAvatar.ImageUrl = "/images/avatars/" + Manager.loggedUser.getAuthType() + ".png";
        try
        {
            string html = File.ReadAllText(Server.MapPath("/Menu/" + Manager.loggedUser.getAuthType() + ".html"));
            subMenu.Text = html;
        }
        catch(FileNotFoundException)
        {

        }
        
        }



    protected void updateNotifications()
    {


        String scriptText = @"
<script>
        $(document).ready(function() {";


        Dictionary<String, Object> sendMap = new Dictionary<string, object>();
        sendMap.Add("userid", Manager.loggedUser.getUserID());
        
        DataTable dt = Manager.runProcWithResults("get_user_notifications", sendMap, false);
        ntCount.Text = dt.Rows.Count.ToString();
        string ltListText = "";
        foreach(DataRow notification in dt.Rows)
        {
            String status = "";
            switch (int.Parse(notification["stat"].ToString()))
            {
                case 0:
                    status = "success";
                    break;
                case 1:
                    status = "error";
                    break;
                case 2:
                    status = "info";
                    break;
                case 3:
                    status = "warning";
                    break;
            }
            ltListText +=@" <li>
                                <div>
                                    <i class='fa fa-envelope fa-fw'></i> "+notification["msg"] + @"
                                     <span class='pull-right text-muted small'>"+status+@"</span>
                                </div>
                            
                        </li>
                       
                        <li class='divider'></li>
                ";

            
            scriptText += "toastr."+status+"(\""+notification["msg"]+"\", \""+notification["strTitle"]+"\");";
        }

        scriptText += @"
    });
    </script>";
        ntList.Text = "<li class='divider'></li>";
        if (dt.Rows.Count == 0)
        {
            ntList.Text = ntList.Text + @"
                
                 <li>
                                <div>
                                    <i class='fa fa-envelope fa-fw'></i> There are no new messages
                                     <span class='pull-right text-muted small'>iGarden</span>
                                </div>
                            
                        </li>
                       
                        <li class='divider'></li>
";
        }else
        ntList.Text = ntList.Text + ltListText;
        Manager.runProcWithResults("update_notification_read_stat", sendMap, false);

        jsNotify.Text = scriptText;

    }
    }

