﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Login" %>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>iGarden | Registration</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">

    <link href="css/plugins/chosen/chosen.css" rel="stylesheet">

    <link href="css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">

    <link href="css/plugins/cropper/cropper.min.css" rel="stylesheet">

    <link href="css/plugins/switchery/switchery.css" rel="stylesheet">

    <link href="css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

    <link href="css/plugins/nouslider/jquery.nouislider.css" rel="stylesheet">

    <link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet">

    <link href="css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">
    <link href="css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet">

    <link href="css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

    <link href="css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

    <link href="css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

    <link href="css/plugins/select2/select2.min.css" rel="stylesheet">

    <link href="css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">



</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown igardenlogo_register">
        <div>
            <div>
                
                <h1 class="logo-name" style="padding-left: 20px; padding-bottom: 10px;font-size: 80px; ">iG+</h1>

            </div>
            <h3>Welcome to iGarden+</h3>

            <p  style="color: red; font-size: 10px;">
                <asp:Literal ID="lblError" runat="server"></asp:Literal>
            </p>
            <form class="m-t" role="form" action="Register.aspx" runat="server">
            
                             <div class="form-group">
                    <!-- ID: regID -->
                    <asp:TextBox  ID="regID" runat="server" CssClass="form-control" placeholder="User ID"  ></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RegularExpressionValidator1" runat="server"
             ErrorMessage="Invalid ID (9 Digits)" ControlToValidate="regID"
                         Display="Dynamic"
             SetFocusOnError="True"
             ValidationExpression="^\d{9}$">
</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RequiredFieldValidator1" runat="server"
             ErrorMessage="Invalid ID (9 Digits)" ControlToValidate="regID"
                        Display="Dynamic"
             SetFocusOnError="True"
             ValidationExpression="^\d{9}$">
</asp:RegularExpressionValidator>
                </div>
               
                <div class="form-group">
                    <!-- pass: regPass -->
                     <asp:TextBox  ID="regPass" type="password" runat="server" CssClass="form-control" placeholder="Password" TextMode="Password" ></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="regPass" runat="server"
                        SetFocusOnError="true" ErrorMessage="Password field cannot be empty"></asp:RequiredFieldValidator>
                    <!--pass: regPass2 -->
                     <asp:TextBox  ID="regPass2" type="password" runat="server" CssClass="form-control" placeholder="Repeat Password" TextMode="Password"></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" 
     ControlToValidate="regPass2"
    
     ControlToCompare="regPass"
                 Display="Dynamic"        
     ErrorMessage="Passwords do not match" 
     ToolTip="Password must be the same" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                          Display="Dynamic"
     ErrorMessage="&laquo; (Required)" 
     ControlToValidate="regPass2"
     CssClass="ValidationError"
     ToolTip="Compare Password is a REQUIRED field">
    </asp:RequiredFieldValidator>
                </div>
                <asp:TextBox ID="regFullName" type="text" runat="server" CssClass="form-control" placeholder="ex. Last First"></asp:TextBox>
         
                    <asp:RegularExpressionValidator ID="nameValidator" runat="server"
             ErrorMessage="Invalid Format (ex. Lev Shakked)" ControlToValidate="regFullName"
                        Display="Dynamic"
             SetFocusOnError="True"
             ValidationExpression="^[a-zA-Z]+[ ]{1,}[a-zA-Z]{1,}">
</asp:RegularExpressionValidator>
                       <asp:RequiredFieldValidator ControlToValidate="regFullName" runat="server" SetFocusOnError="true" ErrorMessage="Name must be entered" Display="Dynamic">

                </asp:RequiredFieldValidator>


                
                <div class="form-group">
 

                        <div class="text-center">
                            
                            
                          
                            
                            </div>
                            
                            <div class="m-r-md inline">
                            
                            </div>
                        </div>
                
                             <table align="center">
                               <tr style="text-align: center;"><td>Day</td><td>Month</td><td>Year</td></tr>
                               <tr style="text-align: center;" ><td style="text-align: center;">
                                   <!-- day: regDateDay -->
                               <input type="text" name="regDateDay" value="31" class="dial m-r-sm" data-fgColor="#1AB394" data-width="50" data-height="50" data-max="31" data-cursor=true data-min="1" data-thickness=.3/>
                                     
                                </td>
                                   <td>
                                       <!-- month: regDateMonth -->
                                       <input type="text" name="regDateMonth" value="1" data-min="1" class="dial m-r" data-fgColor="#1AB394" data-max="12" data-width="60" data-height="60"/>
                                   </td>
                                   <td align="center">
                                       <div class="m-r-md inline">
                                           <!-- year: regDateYear -->
                            <input type="text" value="1989" name="regDateYear" class="dial m-r" data-fgColor="#1AB394" data-width="85" data-height="85" data-step="1" data-min="1950" data-max="2017" data-displayPrevious=true/>
                            </div>
                                   </td>

                               </tr></table>
          
               
               
        
    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>


    <!-- Chosen -->
    <script src="js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="js/plugins/jsKnob/jquery.knob.js"></script>

   

    <!-- IonRangeSlider -->
    <script src="js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

  
    <script>
        $(".dial").knob();
    </script> 

</div>

          

                <asp:Button ID="Button1" CssClass="btn btn-primary block full-width m-b" runat="server" Text="Register iGarden+" />
                

                
                <p class="text-muted text-center"><small>Already have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="/Login.aspx">Log In iGarden+</a>
                <a class="btn btn-sm btn-white btn-block" style="background-color: darkred; color: white;" href="/Admin.aspx">Administrator LogIn</a>
            </form>
            
        </div>
    </div>

    <!-- Mainly scripts -->


</body>

</html>

