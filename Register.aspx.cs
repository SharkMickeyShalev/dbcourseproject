﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Data;
using System.Collections.Specialized;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (IsPostBack)
        {
            NameValueCollection nvc = Request.Form;
            Manager.log("Registration Commited");

            if (!regPass.Text.Equals(regPass2.Text))
            {
                lblError.Text = "Password fields are not equal";
                return;
            }
            String ID = regID.Text;
            String pass = regPass.Text;
   
            String surName = (regFullName.Text.Split())[0];
            String firstName = (regFullName.Text.Split())[0];
            int day = int.Parse(nvc["regDateDay"]);
            int month = int.Parse(nvc["regDateMonth"]);
            int year = int.Parse(nvc["regDateYear"]);
            DateTime convertedDOB = new DateTime(year, month, day);
    

            Manager.log("Registration Details:\nID: " + regID.Text + "\nPassword 1: " + regPass.Text + "\nPassword 2: " + regPass2.Text + "\nDOB (d,m,y): " + day + "," + month + "," + year + ")\nName: " + surName + " " + firstName);
            Manager.log("DOB Format: " + convertedDOB.ToString("dd/MM/yyyy"));
            

            lblError.Text = "";
            String id = regID.Text;

            //Try to execute
            Dictionary<String, Object> a = new Dictionary<String, Object>();
            a.Add("ID", ID);
            a.Add("firstName", firstName);
            a.Add("surName", surName);
            a.Add("dateOfBirth", convertedDOB);
            a.Add("password", pass);

            DataTable returned = Manager.runProcWithResults("add_parent", a);    
            if(returned==null || returned.Rows.Count == 0)
            {
                //There was a fault!
                lblError.Text = "User ID Already exists in the system!";
            }
            else
            {
                Session.Add("user",new global::User(ID, surName + " " + firstName, EAuth.Parent, convertedDOB));
                Response.Redirect("./");
            }

            
            
        }
    }
  
}