/*
CREATE PROC add_parent @ID char(9), @firstName nvarchar(40), 
					   @surName nvarchar(40), @dateOfBirth datetime, @password nvarchar(20)
AS
BEGIN TRANSACTION
INSERT INTO persons VALUES (@ID, @firstName, @surName, @dateOfBirth, @password)
INSERT INTO parents VALUES (@ID)

COMMIT

SELECT ID FROM Persons WHERE ID = @id

GO


CREATE PROC add_kid @ID char(9), @firstName nvarchar(40), 
				 @surName nvarchar(40), @dateOfBirth datetime, @password nvarchar(20) = 0,
				 @street nvarchar(40), @houseNum nvarchar(10), @city nvarchar(40), @lat float, @long float,
				 @placeInFamily int, @kindergardenID int, @classNum int, @parentID char(9)


AS
BEGIN TRANSACTION
INSERT INTO persons VALUES (@ID, @firstName, @surName, @dateOfBirth, @password)
INSERT INTO kids VALUES (@ID, @street, @houseNum, @city, @lat, @long, @placeinfamily, @kindergardenID, @classnum, @parentID)

COMMIT

SELECT kidid FROM kids WHERE kids = @ID

GO



--sign 
CREATE PROC sign_kid_to_activity @ID char(9), @kindergardenID int, @classNumber int, @activityID int
AS
BEGIN TRANSACTION
INSERT INTO Signedfor VALUES (@ID, @kindergardenID, @classNumber, @activityID)

COMMIT

SELECT kidid FROM SignedFor WHERE kidid = @id AND kindergardenid = @kindergardenid AND classNumber = @classnumber AND Activityid = @activityid

GO



--adding training for assitant
CREATE PROC add_training_to_assistant @ID char(9),@trainingID int, @trainingDate datetime
AS
BEGIN TRANSACTION
INSERT INTO TrainingForAssistant VALUES (@ID, @trainingID, @trainingDate)

COMMIT

SELECT assistantid FROM TrainingForAssistant WHERE assistantid = @id AND trainingid = @trainingid

GO



--get list of private gardens for specific kid
CREATE PROC privateGardens_class_list_by_age @dateOfBirth datetime
AS

DECLARE @date datetime
SET @date = '1-9-2010'
SET @date = DATEADD(year,DATEDIFF(year,@date,GETDATE()),@date)

SELECT *
FROM Kindergardens K INNER JOIN Classes C ON K.id = C.kindergardenid
WHERE k.private = 1 AND 
	  DATEDIFF(year ,@dateOfBirth, @date) = c.minimumage

GO




USE [maozako_igarden_project]
GO
/****** Object:  StoredProcedure [maozako_igarden].[getLoginAuth]    Script Date: 2017-07-14 04:24:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [maozako_igarden].[getLoginAuth]
	-- Add the parameters for the stored procedure here
	@strUsername char(9),
	@strPassword nvarchar(50)
	AS
BEGIN
	
	DECLARE @auth int;
SELECT Result.ID, Result.FullName, Result.DOB, 'Auth' = CASE 
			WHEN Result.teacherID is not null THEN 1
			WHEN Result.assistantID is not null THEN 2
			WHEN Result.KidID is not null THEN 3
			WHEN Result.OperatorID is not null THEN 4
			WHEN Result.ParentID is not null THEN 5
			ELSE 0
	END

FROM (SELECT P.ID, P.firstName+' '+P.surName as 'FullName', P.dateOfBirth as 'DOB', T.teacherID, A.assistantID, K.KidID, O.ID as OperatorID, Pa.ParentID as ParentID FROM Persons P left outer join Teachers T ON P.ID=T.teacherID 
left outer join Assistants A ON P.ID=A.assistantID left outer join Kids K ON P.ID=K.KidID left outer join Operators O ON P.ID=O.ID left outer join Parents Pa ON P.ID=Pa.ParentID
WHERE P.ID=cast( @strUsername as char(9)) AND P.[Password]=@strPassword) Result

END




*/