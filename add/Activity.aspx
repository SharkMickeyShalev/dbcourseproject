﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Activity.aspx.cs" Inherits="add_Child" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    Add Opinion
</asp:Content>
<asp:Content ContentPlaceHolderID="head_include" runat="server">
      <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="headerFirst" Runat="Server">
    Opinions
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="headerSecond" Runat="Server">
    Add Opinion
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       <link href="/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
      <link href="/css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">
    <link href="/css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet">

    <link href="/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

    <link href="/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

    <link href="/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

    <link href="/css/animate.css" rel="stylesheet">
      <link href="/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/css/plugins/steps/jquery.steps.css" rel="stylesheet">
     <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Please use the form below to add an opinion</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <form runat="server" method="post" class="form-horizontal">
                                


                                <div class="form-group"><label class="col-sm-2 control-label">Selected Kindergarden</label>
                                    <div class="col-sm-10">                                       
                                        <asp:TextBox  ID="kindername" runat="server" CssClass="form-control" placeholder=" " enabled ="false"  ></asp:TextBox>
                                    </div>
                                </div>


                                <div class="hr-line-dashed"></div>



                                <div class="form-group"><label class="col-sm-2 control-label">Selected Class</label>
                                    <div class="col-sm-10">                                       
                                        <asp:TextBox  ID="classname" runat="server" CssClass="form-control" placeholder=" " enabled ="false"  ></asp:TextBox>
                                    </div>
                                </div>


                                <div class="hr-line-dashed"></div>


                                
                                 <div class="form-group"><label class="col-sm-2 control-label">Select Activity</label>
                                    <div class="col-sm-10">                                     
                                        <asp:DropDownList ID="activity" runat="server" AppendDataBoundItems="true" Height="30" Width ="150">
                                            <asp:ListItem Text="Select Activity" Value="0" /> 
                                        </asp:DropDownList>
                                    </div>
                                </div>


                                <div class="hr-line-dashed"></div>


                                <div class="form-group"><label class="col-sm-2 control-label">Select Operator</label>
                                    <div class="col-sm-10">                                       
                                        <asp:DropDownList ID="operator" runat="server" AppendDataBoundItems="true" Height="30" Width ="150">
                                            <asp:ListItem Text="Select Operator" Value="0" /> 
                                        </asp:DropDownList>                                    

                                    </div>
                                </div>


                                <div class="hr-line-dashed"></div>


                                <div class="form-group"><label class="col-sm-2 control-label">Select Operator</label>
                                    <div class="col-sm-10">                                       
                                        <asp:DropDownList ID="dayinweek" runat="server" AppendDataBoundItems="true" Height="30" Width ="150">
                                            <asp:ListItem Text="Select Operator" Value="0" /> 
                                            <asp:ListItem Text="Sunday" Value="1" />
                                            <asp:ListItem Text="Monday" Value="2" />
                                            <asp:ListItem Text="Tuesday" Value="3" />
                                            <asp:ListItem Text="Wednesday" Value="4" />
                                            <asp:ListItem Text="Thursday" Value="5" />
                                        </asp:DropDownList>                                    

                                    </div>
                                </div>

                                <div class="hr-line-dashed"></div>



                                 <div class="form-group"><label class="col-sm-2 control-label">Select Time</label>
                                    <div class="col-sm-10"> 
                                    <table align="left">
                                    <tr style="text-align: center;" ><td style="text-align: center;">
                                   <!-- day: regDateDay -->
                                    <input type="text"  name="hour" value="14" class="dial m-r-sm" data-fgColor="#1AB394" data-width="50" data-height="50" data-max="20" data-cursor=true data-min="14" data-thickness=.3/>

                                                                           <!-- day: regDateDay -->
                                    <input type="text"  name="min" value="0" class="dial m-r-sm" data-fgColor="#1AB394" data-width="50" data-height="50" data-max="59" data-cursor=true data-min="0" data-thickness=.3/>
                               </tr></table>

                                    </div>
                                </div>

                                <div class="hr-line-dashed"></div>




                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <asp:Button CssClass="btn btn-primary" ID="btnSubmit" runat="server" Text="Add Activity" value="Submitted" />
                                       </form>
                                          
                                    </div>

                                </div>
</form>


                               

                                    <div class="form-group">
                                        <div class="col-lg-5">iGarden ltd.</div>
                                    </div>
                                </div>

                                </div>
                            
                        </div>
                    </div>
                </div>
            </div>
   
</asp:Content>

<asp:Content ContentPlaceHolderID="jsLoad" runat="server">
          
   <script src="/js/jquery-2.1.1.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>



    <!-- Chosen -->
    <script src="/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="/js/plugins/jsKnob/jquery.knob.js"></script>

   

    <!-- IonRangeSlider -->
    <script src="/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

  
    <script>
        $(".dial").knob();
     
    </script>

</asp:Content>