﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Data;
using System.Drawing;
using System.Collections.Specialized;

public partial class add_Child : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {

        Uri unparsedUrl = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
        string query = unparsedUrl.Query;
        var queryParams = HttpUtility.ParseQueryString(query);

        int kindergarden = -1;
        int classnum = -1;

        try
        {
            classnum = int.Parse(queryParams["class"]);
            kindergarden = int.Parse(queryParams["kinder"]);
        }
        catch (Exception exp)
        {
            Manager.log(exp.Message);
        }

        addActivities();
        addOperators();
        kindername.Text = getKindergardenName(kindergarden);
        classname.Text = getClassName(classnum, kindergarden);
        String operID = null;
        int activityID = -1;
        int dayofweek = -1;
        DateTime tm;



        Manager.log(activity + "");

        if (IsPostBack)
        {
            activityID = int.Parse(activity.SelectedItem.Value);
            operID = @operator.SelectedItem.Value;
            NameValueCollection nvc = Request.Form;
            dayofweek = int.Parse(dayinweek.SelectedItem.Value);
            tm = new DateTime(2017, 1, 1, int.Parse(nvc["hour"]), int.Parse(nvc["min"]), 0);
            Manager.log("Trying to add activity with details: oper=" + operID + "activity=" + activityID + "day=" + dayofweek + "time" + tm.ToShortTimeString());


            if (activityID < 0 || operID == null || dayofweek < 1 || tm == null) return;

            //add the activity
            Dictionary<String, Object> toAdd = new Dictionary<string, object>();
            toAdd.Add("kid", kindergarden);
            toAdd.Add("classnum", classnum);
            toAdd.Add("activityid", activityID);
            toAdd.Add("operid", operID);
            toAdd.Add("dayofweek", dayofweek);
            toAdd.Add("startTime", tm);


            DataTable dt = Manager.runProcWithResults("add_activity_to_kindergarden", toAdd);

            if (dt.Rows.Count > 0)
            {
                Manager.addNotification("Added activity to kinderfarden", "Successfully added activity " + activity.SelectedItem.Text + " to kindergarden. " + kindergarden, 0);
                Manager.log("Successfully added activity" +activityID +  " to kindergarden " + kindergarden);
                Response.Redirect("/");
            }
            else
            {
                Manager.addNotification("Couldn't add activity", "We are sorry,<br />But it seems like there is a problem.", 1);
                Manager.log("FATAL ERROR on trying to add activity to kindergarden");
            }







        }
    }




    private String getKindergardenName(int id)
    {
        String kgName = null;
        Dictionary<String, Object> getkg = new Dictionary<string, object>();
        getkg.Add("id", id);

        DataTable dt = Manager.runProcWithResults("get_kindergarden_details", getkg);

        if (dt.Rows.Count > 0)
        {
            kgName = dt.Rows[0]["name"].ToString();
        }


        return kgName;
    }


    private String getClassName(int classnum, int kinder)
    {
        String nm = "ERROR";
        Dictionary<String, Object> cls = new Dictionary<string, object>();
        cls.Add("kinderid", kinder);
        cls.Add("classnum", classnum);

        DataTable dt = Manager.runProcWithResults("get_class_name", cls);

        if (dt.Rows.Count > 0)
        {
            nm = dt.Rows[0]["name"].ToString();
        }


        return nm;
    }


    private void addActivities()
    {
        DataTable dt = Manager.runProcWithResults("get_all_activities");
        //activity.Items.Add("asdad");
        Manager.log("Numer of operators recieved: "+dt.Rows.Count);

        try
        {
            foreach (DataRow row in dt.Rows)
            {
                ListItem lst = new ListItem((String)row["name"], ((int)row["id"]).ToString());
                activity.Items.Add(lst);
            }
        }
        catch (Exception ex)
        {
            Manager.log(ex.Message);
        }

        
    }



    private void addOperators()
    {
        DataTable dt = Manager.runProcWithResults("get_all_operators");
        //activity.Items.Add("asdad");
        Manager.log("Numer of activities recieved: " + dt.Rows.Count);

        try
        {
            foreach (DataRow row in dt.Rows)
            {
                ListItem lst = new ListItem((String)row["name"], (String)row["ID"]);
                @operator.Items.Add(lst);
            }
        }
        catch (Exception ex)
        {
            Manager.log(ex.Message);
        }


    }


    private String getActivityName(int id)
    {
        String acName = null;
        Dictionary<String, Object> getAc = new Dictionary<string, object>();
        getAc.Add("ID", id);

        DataTable dt = Manager.runProcWithResults("get_activity_name", getAc);

        if (dt.Rows.Count > 0)
        {
            acName = dt.Rows[0]["name"].ToString();
        }


        return acName;
    }


}
 