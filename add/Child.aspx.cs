﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Data;
using System.Drawing;
using System.Collections.Specialized;

public partial class add_Child : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {

        if (IsPostBack && Longitude.Text.Length>0)
        {
           

            int KindergardenID = -1;
            int ClassID = -1;
            Manager.log("Lat: " + Latitude.Text + " Long: " + Longitude.Text);
            double SelfKidLat = double.Parse(Latitude.Text);
            double SelfKidLong = double.Parse(Longitude.Text);
            NameValueCollection nvc = Request.Form;
            int day = int.Parse(nvc["regDateDay"]);
            int month = int.Parse(nvc["regDateMonth"]);
            int year = int.Parse(nvc["regDateYear"]);
   
            DateTime convertedDOB = new DateTime(year, month, day);


            if (slctKindergarden.SelectedValue.Equals("public"))
            {
                KindergardenID = getClosestPublicKindergarden(SelfKidLat, SelfKidLong, convertedDOB);
                Manager.log("Minimum kindergarden is " + KindergardenID);
            }
            else
            {
                KindergardenID = int.Parse(chosenKindergarden.SelectedValue);
                Manager.log("Private kindergarden chosen: " + KindergardenID + " " + ClassID);
            }

            ClassID = getClassNumber(KindergardenID, convertedDOB);
            if (KindergardenID == -1 || ClassID == -1)
            {
                Manager.addNotification("Add Child Error", "There is no class suitable for the chosen kindergarden", 1);
                return;

            }

            Dictionary<String, Object> addChildMap = new Dictionary<string, object>();
            addChildMap.Add("ID", childID.Text);
            addChildMap.Add("firstName", regFullName.Text.Split()[1]);
            addChildMap.Add("surName", regFullName.Text.Split()[0]);
            addChildMap.Add("dateOfBirth", convertedDOB);
            addChildMap.Add("password", "none");
            addChildMap.Add("street", Street.Text);
            addChildMap.Add("city", City.Text);
            addChildMap.Add("houseNum", HouseNumber.Text);
            addChildMap.Add("lat", float.Parse(Latitude.Text));
            addChildMap.Add("long", float.Parse(Longitude.Text));
            addChildMap.Add("placeInFamily", int.Parse(placeInFamily.Text));
            addChildMap.Add("kindergardenID", KindergardenID);
            addChildMap.Add("classNum", ClassID);
            addChildMap.Add("parentID", ((User)Session["user"]).getUserID());
            DataTable dt = Manager.runProcWithResults("add_kid", addChildMap);
            if (dt.Rows.Count > 0) {
                Manager.addNotification("Added a Child", "Successfully added " + regFullName.Text + " as your child!", 0);
                Manager.log("Successfully added child " + childID.Text);
                Response.Redirect("/manage/Children.aspx");
                    }
            else
            {
                Manager.addNotification("Couldn't add child", "We are sorry,<br />But it seems like there is a child exists in our system with the same ID.", 1);
                Manager.log("FATAL ERROR on trying to add kid");
            }



        }
        else if(chosenKindergarden.SelectedIndex==-1)
        {
    
            System.Data.DataTable kinderGardens = Manager.runProcWithResults("get_private_kg_list");
            chosenKindergarden.DataSource = kinderGardens;
            chosenKindergarden.DataTextField = "name";
            chosenKindergarden.DataValueField = "ID";
            chosenKindergarden.DataBind();



        }
     
    }

    protected int getClassNumber(int k, DateTime kidBirthDate)
    {
        int toReturn = -1;
        Dictionary<string, object> map = new Dictionary<string, object>();
        map.Add("kid", k);
        map.Add("dateOfBirth", kidBirthDate);
        DataTable dt = Manager.runProcWithResults("get_class_by_age_and_kg", map);
        if (dt.Rows.Count > 0)
        {
            toReturn = (int)dt.Rows[0]["number"];
        }

        return toReturn;
    }

    /**
     * //Select closest garden automatically
     * 
     **/
   protected int getClosestPublicKindergarden(double x, double y, DateTime dob)
    {
        int toReturn = -1;
        Manager.log("Getting public kindergardens list");
        Dictionary<String, Object> toSend = new Dictionary<string, object>();
        toSend.Add("dateofbirth", dob);
        DataTable dt = Manager.runProcWithResults("get_public_kindergardens", toSend);
        Manager.log("Received " + dt.Rows.Count + " public kindergardens.");
        double tmpDistance = 0, minDistance = 0;
        foreach (DataRow row in dt.Rows)
        {
            double rowx = (double)row["latitude"];
            double rowy = (double)row["longitude"];
            //Get closest kindergarden
            tmpDistance = (Math.Pow(rowx - x, 2) + Math.Pow(rowy - y, 2));
            if (toReturn == -1)
            {
                //First
                minDistance = tmpDistance;
                toReturn = int.Parse(row["ID"].ToString());
            }
            if (tmpDistance < minDistance)
            {
                toReturn = int.Parse(row["ID"].ToString());
                minDistance = tmpDistance;
            }

        }
        return toReturn;

    }
 

    protected void updateAddress(object sender, EventArgs e)
    {
        lblAddress.Text = "Validating Address..";
        if (!IsGroupValid("addressValGroup"))
        {
            lblAddress.Text = "Please complete all fields necessary.";

        }
        else
        {
            try
            {
                String city = City.Text;
                String street = Street.Text;
                String houseNo = HouseNumber.Text;
                if(city.Length==0 || street.Length==0 || houseNo.Length == 0){
                    return;
                }
                GoogleGeoCodeResponse res = Manager.GEOCodeAddress(city + ", " + street + " " + houseNo);
                lblAddress.Text = res.results[0].formatted_address + "<br />Latitude: " + res.results[0].geometry.location.lat + " Longitude " + res.results[0].geometry.location.lng;
                Longitude.Text = res.results[0].geometry.location.lng;
                Latitude.Text = res.results[0].geometry.location.lat;
            }
            catch(Exception)
            {
                try
                {
                    GoogleGeoCodeResponse res = Manager.GEOCodeAddress(City.Text);
                    lblAddress.Text = res.results[0].formatted_address + "<br />Latitude: " + res.results[0].geometry.location.lat + " Longitude " + res.results[0].geometry.location.lng;
                    Longitude.Text = res.results[0].geometry.location.lng;
                    Latitude.Text = res.results[0].geometry.location.lat;
                }
                catch (Exception ex2)
                {
                    Manager.log("Couldn't parse google address \n" + ex2.Message);
                    lblAddress.Text = "Couldn't parse google address.";
                }
            }
        }
    }

    protected bool IsGroupValid(string sValidationGroup)
    {
        foreach (BaseValidator validator in Page.Validators)
        {
            if (validator.ValidationGroup == sValidationGroup)
            {
                bool fValid = validator.IsValid;
                if (fValid)
                {
                    validator.Validate();
                    fValid = validator.IsValid;
                    validator.IsValid = true;
                }
                if (!fValid)
                    return false;
            }

        }
        return true;
    }



}
 