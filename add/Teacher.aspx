﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Teacher.aspx.cs" Inherits="add_Teacher" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    Add a Teacher
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="headerFirst" Runat="Server">
    Teachers
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="headerSecond" Runat="Server">
    Add Teacher
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       <link href="/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
      <link href="/css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">
    <link href="/css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet">

    <link href="/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

    <link href="/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

    <link href="/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

    <link href="/css/animate.css" rel="stylesheet">
      <link href="/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/css/plugins/steps/jquery.steps.css" rel="stylesheet">
     <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Please use the form below to add a teacher</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <form runat="server" method="post" class="form-horizontal">
                                <div class="form-group"><label class="col-sm-2 control-label">Teacher ID
                                                        </label>

                                    <div class="col-sm-10">
                                        
                                        <asp:TextBox ID="frmTeacherID" runat="server" CssClass="form-control" placeholder="9 Digit ID"  ></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RegularExpressionValidator1" runat="server"
             ErrorMessage="Invalid ID (9 Digits)" ControlToValidate="frmTeacherID"
                         Display="Dynamic"
             SetFocusOnError="True"
             ValidationExpression="^\d{9}$">
</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RequiredFieldValidator1" runat="server"
             ErrorMessage="Invalid ID (9 Digits)" ControlToValidate="frmTeacherID"
                        Display="Dynamic"
             SetFocusOnError="True"
             ValidationExpression="^\d{9}$">
</asp:RegularExpressionValidator>


                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Upload Image
                                                        </label>

                                    <div class="col-sm-10">
                                     
                                       
                                         <asp:RequiredFieldValidator ControlToValidate="frmGender" runat="server" 
                                             SetFocusOnError="true" ErrorMessage="Image must be uploaded" Display="Dynamic">

                </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Full Name</label>
                                    <div class="col-sm-10">  <asp:TextBox ID="regFullName" type="text" runat="server" CssClass="form-control" placeholder="ex. Last First"></asp:TextBox>
         
                    <asp:RegularExpressionValidator ID="nameValidator" runat="server"
             ErrorMessage="Invalid Format (ex. Lev Shakked)" ControlToValidate="regFullName"
                        Display="Dynamic"
             SetFocusOnError="True"
             ValidationExpression="^[a-zA-Z]+[ ]{1,}[a-zA-Z]{1,}">
</asp:RegularExpressionValidator>
                       <asp:RequiredFieldValidator ControlToValidate="regFullName" runat="server" SetFocusOnError="true" ErrorMessage="Name must be entered" Display="Dynamic">

                </asp:RequiredFieldValidator>


                                    </div>
                                </div>
                                            <div class="form-group"><label class="col-sm-2 control-label">Date of Birth</label>
                                    <div class="col-sm-10"> 
                       <table align="left">
                               <tr style="text-align: center;"><td style="width: 100px;">Day</td><td style="width: 100px;">Month</td><td style="width: 100px;">Year</td></tr>
                               <tr style="text-align: center;" ><td style="text-align: center;">
                                   <!-- day: regDateDay -->
                               <input type="text" name="regDateDay" value="31" class="dial m-r-sm" data-fgColor="#1AB394" data-width="50" data-height="50" data-max="31" data-cursor=true data-min="1" data-thickness=.3/>
                                     
                                </td>
                                   <td>
                                       <!-- month: regDateMonth -->
                                       <input type="text" name="regDateMonth" value="1" data-min="1" class="dial m-r" data-fgColor="#1AB394" data-max="12" data-width="60" data-height="60"/>
                                   </td>
                                   <td align="center">
                       
                                       <div class="m-r-md inline">
                                           <!-- year: regDateYear -->
                            <input type="text" value="1989" name="regDateYear" class="dial m-r" data-fgColor="#1AB394" data-width="85" data-height="85" data-step="1" data-min="1950" data-max="2017" data-displayPrevious=true/>
                            </div>
                                   </td>

                               </tr></table>
          
                    
                      

                                    </div>
                                </div>
                               
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Address</label>

                                    <div class="col-sm-10">
                                        
                                        <asp:TextBox ID="City" CssClass="form-control" style="display: inline; width: 100px;" runat="server" placeholder="City"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" Display="Dynamic" ControlToValidate="City" ErrorMessage="*" ></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator runat="server" Display="Dynamic" ControlToValidate="City" ErrorMessage="Invalid City" 
                                            ValidationExpression="^[a-zA-Z]{1,}$"></asp:RegularExpressionValidator>

                                        <asp:TextBox ID="Street" CssClass="form-control" style="display: inline; width: 100px;" runat="server" placeholder="Street"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" Display="Dynamic" ControlToValidate="Street" ErrorMessage="*" ></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator runat="server" Display="Dynamic" ControlToValidate="Street" ErrorMessage="Invalid Street" 
                                            ValidationExpression="^[a-zA-Z ]{1,}$"></asp:RegularExpressionValidator>

                                        <asp:TextBox ID="HouseNumber" OnTextChanged="updateAddress" AutoPostBack="true" CssClass="form-control" style="display: inline; width: 100px;" runat="server" placeholder="House #"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" Display="Dynamic" ControlToValidate="HouseNumber" ErrorMessage="*" ></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator runat="server" Display="Dynamic" ControlToValidate="HouseNumber" ErrorMessage="Invalid House Number"
                                            ValidationExpression="^[0-9]{1,}$"></asp:RegularExpressionValidator>

                                        <asp:TextBox runat="server" ID="Latitude" CssClass="form-control" style="display: inline; width: 100px;" Enabled="false" placeholder="Latitude"></asp:TextBox>
                                        <asp:TextBox runat="server" ID="Longitude" CssClass="form-control" style="display: inline; width: 100px;" Enabled="false" placeholder="Longitude"></asp:TextBox>
                                        <br /><asp:RequiredFieldValidator runat="server" Display="Dynamic" ControlToValidate="Longitude" ErrorMessage="Address is invalid" ></asp:RequiredFieldValidator>
                                        
                                      <span class="help-block m-b-none">
                                          <img class="img-circle" src="/images/gmap.jpg" />
                                        <asp:Literal ID="lblAddress" runat="server" Text="* iGarden API will dynamically get the location address using Google Maps API"></asp:Literal>
                                          </span>
                                    </div>
                                           

                                    <asp:Literal ID="test" runat="server"></asp:Literal>
                                </div>
                                <asp:ScriptManager ID="ScriptManager1" runat="server">
   </asp:ScriptManager>
                                <div class="hr-line-dashed"></div>
                                  <div class="form-group">
                                      <label class="col-sm-2 control-label">
                                          Choose Kindergarden Method
                                          </label>
                                      <div class="col-sm-10">
                                       
                           
                                          <asp:RadioButtonList ID="slctKindergarden" runat="server">
                                        <asp:ListItem runat="server" Text="Private Kindergarden" Value="private" id="btnPrivate"></asp:ListItem>
                                        <asp:ListItem runat="server" Text="Public Kindergarden" Value="public" id="btnPublic"></asp:ListItem>
                                        
                                    </asp:RadioButtonList>
                                          </div>
                                  </div>
                                    
                              
                                <div class="hr-line-dashed" runat="server"></div>
                                <div id="privatekinder" class="kinderclasspri" runat="server" style="display: none">
                                <div class="form-group"><label class="col-sm-2 control-label">Kindergarden
                                 </label>
                                    <div class="col-sm-10">
             
<asp:ListBox ID="chosenKindergarden" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="updateClasses"></asp:ListBox>
   
                                        
                                        <asp:RequiredFieldValidator runat="server" ID="v1" ControlToValidate="chosenKindergarden" Display="Dynamic"
                                            ErrorMessage="Please choose a kindergarden"></asp:RequiredFieldValidator>
           


                                    </div>
                                </div>
                                  <div class="form-group"><label class="col-sm-2 control-label">Class
                                 </label>
                                    <div class="col-sm-10">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
   <ContentTemplate>
       <asp:ListBox ID="chosenClass" runat="server" CssClass="form-control"  >
                                            <asp:ListItem Text="Please choose a kindergarden"></asp:ListItem>
                                        </asp:ListBox>                                   
   </ContentTemplate>
   <Triggers>
   <asp:AsyncPostBackTrigger ControlID="chosenKindergarden" EventName="SelectedIndexChanged" />
   </Triggers>
</asp:UpdatePanel>
                                      
  
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="chosenClass" ID="v2" Display="Dynamic"
                                            ErrorMessage="Please choose a class"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                    </div>
                             
                                <div id="publicKinder" class="kinderclasspub" runat="server">
                                    
                                <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-lg-5">Kindergarden will be assigned automatically based on your location.</div>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <asp:Button CssClass="btn btn-primary" ID="btnSubmit" runat="server" Text="Add Kid" value="Submitted" />
                                       
                                          
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
         

    <!-- Mainly scripts -->
    <script src="/js/jquery-2.1.1.js"></script>


    <!-- Chosen -->
    <script src="/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="/js/plugins/jsKnob/jquery.knob.js"></script>

   

    <!-- IonRangeSlider -->
    <script src="/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

  
    <script>
        $(".dial").knob();
        $(document).ready(function () {
            $(".kinderclasspub").hide();
            $(".kinderclasspri").hide();

   
            $("#btnPrivate").click(function () {
                showStep(1);
            });
            $("#btnPublic").click(function () {
                showStep(2);
            });
        });
        function showStep(i) {
            $(".kinderclasspub").hide();
            $(".kinderclasspri").hide();
             document.getElementById("<%=v1.ClientID %>").enabled = false;
                document.getElementById("<%=v2.ClientID %>").enabled = false;
            
            if (i == 1) {
                document.getElementById("<%=v1.ClientID %>").enabled = true;
                document.getElementById("<%=v2.ClientID %>").enabled = true;
                $(".kinderclasspri").show();
            }
            else {
                document.getElementById("<%=v1.ClientID %>").enabled = false;
                document.getElementById("<%=v2.ClientID %>").enabled = false;

                $(".kinderclasspub").show();
            }
            
        }
    </script>


</asp:Content>

