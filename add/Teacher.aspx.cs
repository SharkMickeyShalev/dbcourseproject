﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Data;
using System.Drawing;
using System.Collections.Specialized;

public partial class add_Teacher : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

       

        if (IsPostBack && Latitude.Text.Length!=0)
        {

            int KindergardenID = -1;
            int ClassID = -1;
            double SelfKidLat = double.Parse(Latitude.Text);
            double SelfKidLong = double.Parse(Longitude.Text);
            NameValueCollection nvc = Request.Form;
            String day = nvc["regDateDay"];
            String month = nvc["regDateMonth"];
            String year = nvc["regDateYear"];
            string fd = string.Concat(month, "-", day, "-", year);

            DateTime convertedDOB = Convert.ToDateTime(fd);


            if (slctKindergarden.SelectedValue.Equals("public"))
            {
                KindergardenID = getClosestPublicKindergarden(SelfKidLat, SelfKidLong);
                ClassID = getClassNumber(KindergardenID);
                Manager.log("Minimum kindergarden is " + KindergardenID);


            }
            else
            {
                
                KindergardenID = int.Parse(chosenKindergarden.SelectedValue);
                ClassID = int.Parse(chosenClass.SelectedValue);
                Manager.log("Private kindergarden chosen: " + KindergardenID + " " + ClassID);
            }



            Dictionary<String, Object> addChildMap = new Dictionary<string, object>();
            addChildMap.Add("ID", frmTeacherID.Text);
            addChildMap.Add("firstName", regFullName.Text.Split()[1]);
            addChildMap.Add("surName", regFullName.Text.Split()[0]);
            addChildMap.Add("dateOfBirth", convertedDOB);
            addChildMap.Add("password", "none");
            addChildMap.Add("street", Street.Text);
            addChildMap.Add("city", City.Text);
            addChildMap.Add("houseNum", HouseNumber.Text);
            addChildMap.Add("lat", float.Parse(Latitude.Text));
            addChildMap.Add("long", float.Parse(Longitude.Text));
            
            addChildMap.Add("kindergardenID", KindergardenID);
            addChildMap.Add("classNum", ClassID);
            addChildMap.Add("parentID", ((User)Session["user"]).getUserID());
            DataTable dt = Manager.runProcWithResults("add_kid", addChildMap);
            if (dt.Rows.Count > 0) {

                Manager.log("Successfully added child " + frmTeacherID.Text);
                Response.Redirect("/manage/Children.aspx");
                    }
            else
            {
                Manager.log("FATAL ERROR on trying to add kid");
            }



        }
        else
        {

            System.Data.DataTable kinderGardens = Manager.runProcWithResults("get_kingergarden_list_id");
            chosenKindergarden.DataSource = kinderGardens;
            chosenKindergarden.DataTextField = "name";
            chosenKindergarden.DataValueField = "ID";
            chosenKindergarden.DataBind();



        }
     
    }

    protected int getClassNumber(int k)
    {
        int toReturn = -1;
        Dictionary<string, object> map = new Dictionary<string, object>();
        map.Add("KindergardenID", k);
        DataTable dt = Manager.runProcWithResults("get_class_to_sign", map);
        if (dt.Rows.Count > 0)
        {
            toReturn = (int)dt.Rows[0]["ClassID"];
        }

        return toReturn;
    }

    /**
     * //Select closest garden automatically
     * 
     **/
   protected int getClosestPublicKindergarden(double x, double y)
    {
        int toReturn = -1;
        Manager.log("Getting public kindergardens list");
        DataTable dt = Manager.runProcWithResults("get_public_kindergardens");
        Manager.log("Received " + dt.Rows.Count + " public kindergardens.");
        double tmpDistance = 0, minDistance = 0;
        foreach (DataRow row in dt.Rows)
        {
            double rowx = (double)row["latitude"];
            double rowy = (double)row["longitude"];
            //Get closest kindergarden
            tmpDistance = (Math.Pow(rowx - x, 2) + Math.Pow(rowy - y, 2));
            if (toReturn == -1)
            {
                //First
                minDistance = tmpDistance;
                toReturn = int.Parse(row["ID"].ToString());
            }
            if (tmpDistance < minDistance)
            {
                toReturn = int.Parse(row["ID"].ToString());
                minDistance = tmpDistance;
            }

        }
        return toReturn;

    }
    protected void updateClasses(object sender, EventArgs e)
    {
        //proc name - get_classes_by_kindergarden
        
        chosenClass.Items.Clear();
        
        Manager.log("Kindergarden event updateClasses fired up!");
        Dictionary<String, Object> map = new Dictionary<string, object>();
        map.Add("KindergardenID", chosenKindergarden.SelectedValue);
        DataTable dt = Manager.runProcWithResults("get_classes_by_kindergarden", map);
        chosenClass.DataSource = dt;
        chosenClass.DataTextField = "name";
        chosenClass.DataValueField = "number";
        chosenClass.DataBind();

        
        Manager.log("Found " + dt.Rows.Count + " Classes for kindergarden "+chosenKindergarden.SelectedValue);
        
    }

    protected void updateAddress(object sender, EventArgs e)
    {
        lblAddress.Text = "Validating Address..";
        if (!IsGroupValid("addressValGroup"))
        {
            lblAddress.Text = "Please complete all fields necessary.";

        }
        else
        {
            String city = City.Text;
            String street = Street.Text;
            String houseNo = HouseNumber.Text;
            GoogleGeoCodeResponse res = Manager.GEOCodeAddress(city + ", " + street + " " + houseNo);
            lblAddress.Text = res.results[0].formatted_address+"<br />Latitude: " + res.results[0].geometry.location.lat + " Longitude " + res.results[0].geometry.location.lng;
            Longitude.Text = res.results[0].geometry.location.lng;
            Latitude.Text = res.results[0].geometry.location.lat;

        }
    }

    protected bool IsGroupValid(string sValidationGroup)
    {
        foreach (BaseValidator validator in Page.Validators)
        {
            if (validator.ValidationGroup == sValidationGroup)
            {
                bool fValid = validator.IsValid;
                if (fValid)
                {
                    validator.Validate();
                    fValid = validator.IsValid;
                    validator.IsValid = true;
                }
                if (!fValid)
                    return false;
            }

        }
        return true;
    }



}
 