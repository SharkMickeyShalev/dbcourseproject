﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Admin.aspx.cs" Inherits="Login" %>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>iGarden | Administrator Login</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown igardenlogo_admin">
        <div>
            <div>
                
                 <h1 class="logo-name" style="padding-left: 20px; padding-bottom: 10px;font-size: 80px; ">iG+</h1>

            </div>
            <h3>iGarden Administration</h3>
            
            <p  style="color: red; font-size: 10px;">
                <asp:Literal ID="lblError" runat="server"></asp:Literal>
            </p>
            <form name="adminForm" class="m-t" role="form" action="Admin.aspx" runat="server">
             
                <div class="form-group">
                     <asp:TextBox  ID="logPassword" TextMode="Password" type="password" runat="server" CssClass="form-control" placeholder="Administrator Password" ></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="logPassword" runat="server"
                        SetFocusOnError="true" ErrorMessage="Password field cannot be empty"></asp:RequiredFieldValidator>
                </div>
                <asp:Button ID="Button1" CssClass="btn btn-primary block full-width m-b" runat="server" Text="Log In iGarden+" />
                

                <a href="callto:1-800-IGARDEN"><small>Forgot password? Call 1-800-IGARDEN</small></a>
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="/Register.aspx">Create an account</a>
                <a class="btn btn-sm btn-white btn-block" href="/Login.aspx">Parents Login</a>
            </form>
            
            <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>

</html>

