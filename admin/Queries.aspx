﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Queries.aspx.cs" Inherits="add_Child" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    Delete All Data
</asp:Content>
<asp:Content ContentPlaceHolderID="head_include" runat="server">
     <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Sweet Alert-->
    <link href="/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="headerFirst" Runat="Server">
    Data
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="headerSecond" Runat="Server">
    Delete Data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>You may use this form to delete data</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <form runat="server" method="post" class="form-horizontal">
                                 <div class="form-group"><label class="col-sm-2 control-label">Select Query</label>
                                    <div class="col-sm-10">                                     
                                        <asp:DropDownList ID="activity" runat="server" AppendDataBoundItems="true" Height="30" Width ="300" AutoPostBack="true">
   
                                            <asp:ListItem Text="Select Query" Value="0" />
                                            <asp:ListItem Text="Free Assistants For Activity" Value="q_free_assistants_for_activity" />
                                            <asp:ListItem Text="Heavy Employees Details" Value="q_heavy_emps_details" />
                                            <asp:ListItem Text="Heavy Master Assistants" Value="q_heavy_master_assistants" />
                                            <asp:ListItem Text="Kid's All Activities" Value="q_kids_all_activities" />
                                            <asp:ListItem Text="Kid's All Activities In Overloaded Classes" Value="q_kids_all_activities_in_overload_classes" />
                                            <asp:ListItem Text="Kindergardens Activities" Value="q_kindergarden_activities" />
                                            <asp:ListItem Text="Kindergarden's Top 5 Activities" Value="q_kindergarden_top5_activities" />
                                            <asp:ListItem Text="Payment Per Kid Including Activity Discount" Value="q_payment_per_kid" />
                                            <asp:ListItem Text="Get All Users Details" Value="get_all_persons_details" />

                                            
                                        </asp:DropDownList>
                                        
                                    </div>
                                </div>

                                      </form>

                        
                            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Query Processed Results</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                 <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">



<table class="table table-striped table-bordered table-hover dataTables-example dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info" role="grid">
<thead>
   <tr role="row">
       <asp:Literal ID="columnLT" runat="server"></asp:Literal>
       <!--
      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" style="width: 287px;">Rendering engine</th>
      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 354px;">Browser</th>
      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 320px;">Platform(s)</th>
      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 247px;">Engine version</th>
      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column descending" style="width: 179px;" aria-sort="ascending">CSS grade</th>
       <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column descending" style="width: 179px;" aria-sort="ascending">CSS grade</th>
       <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column descending" style="width: 179px;" aria-sort="ascending">CSS grade</th>
       -->
   </tr>
                    </thead>
                    <tbody>
                        <asp:Literal ID="rowLT" runat="server"></asp:Literal>
                

                 </tbody>
                    
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>


                    <div class="hr-line-dashed" runat="server"></div>
                    
                            <asp:Literal ID="tblDisplay" runat="server"></asp:Literal>



                    <div class="hr-line-dashed" runat="server"></div>

                                    <div class="form-group">
                                        <div class="col-lg-5">
                                        <asp:Label ID="notice" runat="server" Text="Label" ForeColor="black" Font-Bold="false">iGarden ltd.</asp:Label>
                                    </div>
                                        </div>
                                </div>

         </div>

         


</asp:Content>

<asp:Content ContentPlaceHolderID="jsLoad" runat="server">
 
    <!-- Mainly scripts -->
    <script src="/js/jquery-2.1.1.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="/js/plugins/jeditable/jquery.jeditable.js"></script>

    <script src="/js/plugins/dataTables/datatables.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="/js/inspinia.js"></script>
    <script src="/js/plugins/pace/pace.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

            /* Init DataTables */
            var oTable = $('#editable').DataTable();

            /* Apply the jEditable handlers to the table */
            oTable.$('td').editable( '../example_ajax.php', {
                "callback": function( sValue, y ) {
                    var aPos = oTable.fnGetPosition( this );
                    oTable.fnUpdate( sValue, aPos[0], aPos[1] );
                },
                "submitdata": function ( value, settings ) {
                    return {
                        "row_id": this.parentNode.getAttribute('id'),
                        "column": oTable.fnGetPosition( this )[2]
                    };
                },

                "width": "90%",
                "height": "100%"
            } );


        });

        function fnClickAddRow() {
            $('#editable').dataTable().fnAddData( [
                "Custom row",
                "New row",
                "New row",
                "New row",
                "New row" ] );

        }
    </script>

</asp:Content>