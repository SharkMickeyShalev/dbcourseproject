﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Data;
using System.Drawing;
using System.Collections.Specialized;

public partial class add_Child : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
   
        if (IsPostBack)
        {
          
                Manager.log("RUNNING: " + activity.SelectedValue);
                runQuery(activity.SelectedValue);
          
              
            
                
            
        }
     
    }




    private void runQuery(String query)
    {
        //String nm = "ERROR";
        //Dictionary<String, Object> toSend = new Dictionary<string, object>();

        DataTable dt = Manager.runProcWithResults(query);

        if (dt.Rows.Count > 0)
        {
            Manager.log("SUCCESSFUL PROC");
            ConvertDataTableToHTML(dt);
        }else
        {
            columnLT.Text = "<th class='sorting' tabindex='0' aria-controls='DataTables_Table_0' rowspan='1' colspan='1' aria-label='Rendering engine: activate to sort column ascending' style='width: 287px;'>Results will be shown here</th>";
            rowLT.Text = "";
        }

    }



    public void ConvertDataTableToHTML(DataTable dt)
    {
        Manager.log("PRINTING TABLE");
        string columnDataAdd = "";
     

      
        //add header row
        
        for (int i = 0; i < dt.Columns.Count; i++)
            columnDataAdd+=@"<th class='sorting' tabindex='0' aria-controls='DataTables_Table_0' rowspan='1' colspan='1' aria-label='Rendering engine: activate to sort column ascending' style='width: 287px;'>" + dt.Columns[i].ColumnName + "</th>";

        string rowDataAdd = "";
        columnLT.Text = columnDataAdd;
        //add rows
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            rowDataAdd += "<tr>";
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                rowDataAdd += "<td class=''>" + dt.Rows[i][j].ToString() + "</td>";
            }
            rowDataAdd += "</tr>";
        }
        //Manager.log(rowDataAdd);
        rowLT.Text = rowDataAdd;
       
    }

    


}
 