﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Reset.aspx.cs" Inherits="add_Child" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    Delete All Data
</asp:Content>
<asp:Content ContentPlaceHolderID="head_include" runat="server">
     <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Sweet Alert-->
    <link href="/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="headerFirst" Runat="Server">
    Data
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="headerSecond" Runat="Server">
    Delete Data
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>You may use this form to delete data</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <form runat="server" method="post" class="form-horizontal">
                                
                                <div class="form-group"><label class="col-sm-2 control-label">Delete Authorization </label>

                                    <div class="col-sm-10">
                                       
                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" Font-Size="12"> 
                                            <asp:ListItem Text="OK" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Cancel" Value="0" Selected="True"></asp:ListItem>
                                        </asp:RadioButtonList>

                                    </div>
                                </div>



                                <div class="hr-line-dashed" runat="server"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <asp:Button CssClass="btn btn-primary" ID="btnSubmit" runat="server" Text="Delete Data" value="Submitted" OnClientClick="return confirm('Are you sure?');" />
                                       </form>
                                          
                                    </div>

                                </div>


                               

                                    <div class="form-group">
                                        <div class="col-lg-5">
                                        <asp:Label ID="notice" runat="server" Text="Label" ForeColor="Red" Font-Bold="true">NOTICE: ALL OF YOUR DATA WILL BE LOST!!!</asp:Label>
                                    </div>
                                        </div>
                                </div>
         </div>

         

    <!-- Mainly scripts -->
    <script src="/js/jquery-2.1.1.js"></script>


    <!-- Chosen -->
    <script src="/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="/js/plugins/jsKnob/jquery.knob.js"></script>

   

    <!-- IonRangeSlider -->
    <script src="/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>



        <script>
        $(".dial").knob();
     
    </script>

</asp:Content>

<asp:Content ContentPlaceHolderID="jsLoad" runat="server">
          
   <script src="/js/jquery-2.1.1.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>



    <!-- Chosen -->
    <script src="/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="/js/plugins/jsKnob/jquery.knob.js"></script>

        <!-- Sweet alert -->
    <script src="/js/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- IonRangeSlider -->
    <script src="/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

  
    <script>
        $(".dial").knob();
     
    </script>



    <script>
        function doConfirm() {
            swal({
                title: 'Are you sure?',
                text: 'You will not be able to recover this information!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false
            },
                function () {
                    var delayMillis = 2000;
                    swal('Data Deleted', '" + @" Data was deleted from iGarden+ ', 'success');
                    setTimeout(function () {
                        //your code to be executed after 1 second

                        window.location = ' + @"'
                    }, delayMillis);
                });
        }
        </script>



</asp:Content>