﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Data;
using System.Drawing;
using System.Collections.Specialized;

public partial class add_Child : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            DateTime d = DateTime.Today;
            Manager.log(d.Month +"  " + d.ToString());
            if (d.Month > 9 || d.Month < 7)
            {
                Manager.addNotification("Data deletion was blocked", "We are sorry,<br />But data can be deleted only in summer time", 1);
                Manager.log("Error on trying delete data - can delete only in summer time (7-8)");
            }


            int authorize = int.Parse(RadioButtonList1.SelectedValue);
            if (authorize == 0)
            {
                return;
            }
            else if (authorize == 1)
            {
                executeDelete(authorize);
            }
            else
            {
                Manager.addNotification("Couldn't delete data", "We are sorry,<br />But it seems like there is a problem.", 1);
                Manager.log("Error on trying delete data - couldn't find auth value");
            }
        }
     
    }


    private void executeDelete(int auth)
    {

        //NOTE: IN ORDER TO DELETE DATA USE authorize = 1 (for now it was set to 0)
        auth = 0; //<--------------------------------------------------------------


        Dictionary<String, Object> param = new Dictionary<string, object>();
        param.Add("authorize", auth);

        DataTable dt = Manager.runProcWithResults("delete_data", param);
        if (dt.Rows.Count > 0)
        {
            Manager.addNotification("Data was deleted successfully" ,"Your data base is ready for new year" , 0);
            Manager.log("");
            //Response.Redirect("/");
        }
        else
        {
            Manager.addNotification("Couldn't delete data", "We are sorry,<br />But it seems like there is a problem.", 1);
            Manager.log("FATAL ERROR on trying delete data");
        }

    }



}
 