﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Data;
using System.Drawing;
using System.Collections.Specialized;

public partial class add_Child : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
               
        if (IsPostBack)
        {
            Response.Redirect("/manage/Children.aspx?ParentID=" + parent.SelectedValue);


        }else
        {
            runQuery();
        }
     
    }




    private void runQuery()
    {


        DataTable dt = Manager.runProcWithResults("get_parents");

        if (dt.Rows.Count > 0)
        {
            parent.Items.Add("Select a Parent");
            foreach (DataRow row in dt.Rows)
            {
                parent.Items.Add(new ListItem(row["firstname"] + " " + row["surname"],row["id"].ToString()));
            }
        }else
        {
            parent.Items.Add("No Parents in Database");
            parent.Enabled = false;
            
        }

    }
    

    


}
 