﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Xml;

public partial class admin_import : System.Web.UI.Page
{
    Dictionary<int, String> trainingMap = new Dictionary<int, string>();
    List<AJA> AddAJAList = new List<AJA>();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (IsPostBack)
        {
            Manager.log("\n------------------------------\nXML File Uploaded: " + xmlFile.FileName);
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xmlFile.PostedFile.InputStream);
            XmlNodeList TrainingsDetails = xmlDoc.GetElementsByTagName("TrainingsDetails");

         

                /** Parse Trainings from XML **/
                foreach (XmlNode node in TrainingsDetails)
                    foreach (XmlNode innerNode in node.ChildNodes)
                        trainingMap.Add(int.Parse(innerNode.Attributes[0].InnerText), innerNode.InnerText);



                /** Parse AssistantTrainings from XML **/
                XmlNodeList AssistantTrainings = xmlDoc.GetElementsByTagName("AssistantTraining");
                foreach (XmlNode node in AssistantTrainings)
                    foreach (XmlNode innerNode in node.ChildNodes)
                    {
                        string AssistantID = innerNode.Attributes["AssistantID"].InnerText;
                        DateTime dateTrained = DateTime.Parse(innerNode.Attributes["Date"].InnerText);
                        int TrainingID = int.Parse(innerNode.Attributes["TrainingID"].InnerText);
                        AddAJAList.Add(new AJA(AssistantID, dateTrained, TrainingID));
                        Manager.log("Assistant: " + AssistantID + " Date: " + dateTrained.AddDays(1.0) + " Training #" + TrainingID);
                    }




                truncateTrainings();
                addData();
            }

            catch (Exception)
            {
                Manager.log("Could not parse XML File correctly..");
                Manager.log("Rollbacking..");
                truncateTrainings();
                Manager.addNotification("Training data was not generated", "Unfortunately there was a problem parsing your XML Imported training data.", 1);
                return;
            }
            Manager.addNotification("Training data imported!", "We have successfully imported your training data.", 0);
        }
        }
    public class AJA
    {
        string AssistantID;
        DateTime dateTrained;
        int TrainingID;
        public AJA(string AssistantID, DateTime dateTrained, int TrainingID)
        {
            this.AssistantID = AssistantID;
            this.dateTrained = dateTrained;
            this.TrainingID = TrainingID;
        }

        public Dictionary<String, Object> getMap()
        {
            Dictionary<String, Object> toReturn = new Dictionary<string, object>();
            toReturn.Add("ID", AssistantID);
            toReturn.Add("trainingID", TrainingID);
            toReturn.Add("trainingDate", dateTrained);
            return toReturn;
        }
    }

    protected void addData()
    {
        //Attempt to add Trainings
        foreach(KeyValuePair<int, String> entry in trainingMap)
        {
            Dictionary<String, Object> trainingDic = new Dictionary<string, object>();
            trainingDic.Add("id", entry.Key);
            trainingDic.Add("name", entry.Value);
            Manager.runProcWithResults("add_training", trainingDic);
        }


        //Attempt to add Training Data
        foreach(AJA node in AddAJAList)
        {
            Manager.runProcWithResults("add_training_to_assistant", node.getMap());
        }
    }

    protected void truncateTrainings()
    {
        //Truncate Tables
        Manager.log("Truncating training data");
        Dictionary<String, Object> authMap = new Dictionary<string, object>();
        authMap.Add("authorize", 1);
        Manager.runProcWithResults("delete_all_training_data", authMap);
    }
}

