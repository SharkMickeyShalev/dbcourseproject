/****** Object:  Trigger [kidToClassCheck]    Script Date: 7/14/2017 17:02:11 ******/
DROP TRIGGER [dbo].[kidToClassCheck]
GO
/****** Object:  Trigger [checkAssistantInActivity]    Script Date: 7/14/2017 17:02:11 ******/
DROP TRIGGER [dbo].[checkAssistantInActivity]
GO
ALTER TABLE [dbo].[TrainingParameters] DROP CONSTRAINT [CK__TrainingParamete__76969D2E]
GO
ALTER TABLE [dbo].[TakesPlace] DROP CONSTRAINT [CK__TakesPlac__dayIn__5165187F]
GO
ALTER TABLE [dbo].[Shifts] DROP CONSTRAINT [CK__Shifts__shiftTyp__619B8048]
GO
ALTER TABLE [dbo].[Shifts] DROP CONSTRAINT [CK__Shifts__dayInWee__60A75C0F]
GO
ALTER TABLE [dbo].[Persons] DROP CONSTRAINT [CK__Persons__ID__3A81B327]
GO
ALTER TABLE [dbo].[Opinions] DROP CONSTRAINT [CK__Opinions__grade__6FE99F9F]
GO
ALTER TABLE [dbo].[Opinions] DROP CONSTRAINT [CK__Opinions__6EF57B66]
GO
ALTER TABLE [dbo].[Operators] DROP CONSTRAINT [CK__Operators__ID__4BAC3F29]
GO
ALTER TABLE [dbo].[Kindergardens] DROP CONSTRAINT [CK__Kindergar__price__403A8C7D]
GO
ALTER TABLE [dbo].[Classes] DROP CONSTRAINT [CK__Classes__number__48CFD27E]
GO
ALTER TABLE [dbo].[Classes] DROP CONSTRAINT [CK__Classes__minimum__47DBAE45]
GO
ALTER TABLE [dbo].[Classes] DROP CONSTRAINT [CK__Classes__maximum__46E78A0C]
GO
ALTER TABLE [dbo].[Classes] DROP CONSTRAINT [CK__Classes__45F365D3]
GO
ALTER TABLE [dbo].[Activities] DROP CONSTRAINT [CK__Activities__cost__37A5467C]
GO
ALTER TABLE [dbo].[Activities] DROP CONSTRAINT [CK__Activitie__lengt__36B12243]
GO
ALTER TABLE [dbo].[TrainingForAssistant] DROP CONSTRAINT [FK__TrainingF__train__59FA5E80]
GO
ALTER TABLE [dbo].[TrainingForAssistant] DROP CONSTRAINT [FK__TrainingF__assis__59063A47]
GO
ALTER TABLE [dbo].[Teachers] DROP CONSTRAINT [FK__Teachers__teache__3D5E1FD2]
GO
ALTER TABLE [dbo].[TakesPlace] DROP CONSTRAINT [FK__TakesPlace__5070F446]
GO
ALTER TABLE [dbo].[TakesPlace] DROP CONSTRAINT [FK__TakesPlac__opera__4F7CD00D]
GO
ALTER TABLE [dbo].[TakesPlace] DROP CONSTRAINT [FK__TakesPlac__activ__4E88ABD4]
GO
ALTER TABLE [dbo].[SignedFor] DROP CONSTRAINT [FK__SignedFor__kidID__73BA3083]
GO
ALTER TABLE [dbo].[SignedFor] DROP CONSTRAINT [FK__SignedFor__72C60C4A]
GO
ALTER TABLE [dbo].[Opinions] DROP CONSTRAINT [FK__Opinions__kinder__6E01572D]
GO
ALTER TABLE [dbo].[Opinions] DROP CONSTRAINT [FK__Opinions__activi__6D0D32F4]
GO
ALTER TABLE [dbo].[Kids] DROP CONSTRAINT [FK__Kids__kidID__6A30C649]
GO
ALTER TABLE [dbo].[Kids] DROP CONSTRAINT [FK__Kids__693CA210]
GO
ALTER TABLE [dbo].[Classes] DROP CONSTRAINT [FK__Classes__teacher__44FF419A]
GO
ALTER TABLE [dbo].[Classes] DROP CONSTRAINT [FK__Classes__kinderg__440B1D61]
GO
ALTER TABLE [dbo].[AssistIn] DROP CONSTRAINT [FK__AssistIn__shiftN__66603565]
GO
ALTER TABLE [dbo].[AssistIn] DROP CONSTRAINT [FK__AssistIn__assist__656C112C]
GO
ALTER TABLE [dbo].[AssistIn] DROP CONSTRAINT [FK__AssistIn__6477ECF3]
GO
ALTER TABLE [dbo].[Assistants] DROP CONSTRAINT [FK__Assistant__assis__5441852A]
GO
ALTER TABLE [dbo].[AssistantJoinsActivity] DROP CONSTRAINT [FK__AssistantJoinsAc__5DCAEF64]
GO
ALTER TABLE [dbo].[AssistantJoinsActivity] DROP CONSTRAINT [FK__AssistantJoinsAc__5CD6CB2B]
GO
/****** Object:  Table [dbo].[TrainingParameters]    Script Date: 7/14/2017 17:02:11 ******/
DROP TABLE [dbo].[TrainingParameters]
GO
/****** Object:  Table [dbo].[TrainingForAssistant]    Script Date: 7/14/2017 17:02:11 ******/
DROP TABLE [dbo].[TrainingForAssistant]
GO
/****** Object:  Table [dbo].[Training]    Script Date: 7/14/2017 17:02:11 ******/
DROP TABLE [dbo].[Training]
GO
/****** Object:  Table [dbo].[Teachers]    Script Date: 7/14/2017 17:02:11 ******/
DROP TABLE [dbo].[Teachers]
GO
/****** Object:  Table [dbo].[TakesPlace]    Script Date: 7/14/2017 17:02:11 ******/
DROP TABLE [dbo].[TakesPlace]
GO
/****** Object:  Table [dbo].[SignedFor]    Script Date: 7/14/2017 17:02:11 ******/
DROP TABLE [dbo].[SignedFor]
GO
/****** Object:  Table [dbo].[Shifts]    Script Date: 7/14/2017 17:02:11 ******/
DROP TABLE [dbo].[Shifts]
GO
/****** Object:  Table [dbo].[Persons]    Script Date: 7/14/2017 17:02:11 ******/
DROP TABLE [dbo].[Persons]
GO
/****** Object:  Table [dbo].[Opinions]    Script Date: 7/14/2017 17:02:11 ******/
DROP TABLE [dbo].[Opinions]
GO
/****** Object:  Table [dbo].[Operators]    Script Date: 7/14/2017 17:02:11 ******/
DROP TABLE [dbo].[Operators]
GO
/****** Object:  Table [dbo].[Kindergardens]    Script Date: 7/14/2017 17:02:11 ******/
DROP TABLE [dbo].[Kindergardens]
GO
/****** Object:  Table [dbo].[Kids]    Script Date: 7/14/2017 17:02:11 ******/
DROP TABLE [dbo].[Kids]
GO
/****** Object:  Table [dbo].[Classes]    Script Date: 7/14/2017 17:02:11 ******/
DROP TABLE [dbo].[Classes]
GO
/****** Object:  Table [dbo].[AssistIn]    Script Date: 7/14/2017 17:02:11 ******/
DROP TABLE [dbo].[AssistIn]
GO
/****** Object:  Table [dbo].[Assistants]    Script Date: 7/14/2017 17:02:11 ******/
DROP TABLE [dbo].[Assistants]
GO
/****** Object:  Table [dbo].[AssistantJoinsActivity]    Script Date: 7/14/2017 17:02:11 ******/
DROP TABLE [dbo].[AssistantJoinsActivity]
GO
/****** Object:  Table [dbo].[Activities]    Script Date: 7/14/2017 17:02:11 ******/
DROP TABLE [dbo].[Activities]
GO
/****** Object:  Table [dbo].[Activities]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Activities](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](20) NOT NULL,
	[length] [tinyint] NOT NULL,
	[cost] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AssistantJoinsActivity]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssistantJoinsActivity](
	[kindergardenID] [int] NOT NULL,
	[classNumber] [int] NOT NULL,
	[activityID] [int] NOT NULL,
	[assistantID] [char](9) NOT NULL,
	[trainingID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[kindergardenID] ASC,
	[classNumber] ASC,
	[activityID] ASC,
	[assistantID] ASC,
	[trainingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Assistants]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Assistants](
	[assistantID] [char](9) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[assistantID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AssistIn]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssistIn](
	[assistantID] [char](9) NOT NULL,
	[kindergardenID] [int] NOT NULL,
	[classNumber] [int] NOT NULL,
	[shiftNum] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[assistantID] ASC,
	[kindergardenID] ASC,
	[classNumber] ASC,
	[shiftNum] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Classes]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Classes](
	[kindergardenID] [int] NOT NULL,
	[number] [int] NOT NULL,
	[name] [nvarchar](20) NOT NULL,
	[minimumAge] [tinyint] NOT NULL,
	[maximumAge] [tinyint] NOT NULL,
	[maximumKids] [tinyint] NOT NULL,
	[teacher] [char](9) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[kindergardenID] ASC,
	[number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[teacher] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Kids]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kids](
	[kidID] [char](9) NOT NULL,
	[street] [nvarchar](20) NOT NULL,
	[houseNumber] [nvarchar](5) NULL,
	[city] [nvarchar](20) NOT NULL,
	[latitude] [float] NOT NULL,
	[longitude] [float] NOT NULL,
	[fatherName] [nvarchar](20) NULL,
	[motherName] [nvarchar](20) NULL,
	[placeInFamily] [tinyint] NOT NULL,
	[kindergardenID] [int] NOT NULL,
	[classNumber] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[kidID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Kindergardens]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kindergardens](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](20) NOT NULL,
	[street] [nvarchar](20) NOT NULL,
	[houseNumber] [nvarchar](5) NULL,
	[city] [nvarchar](20) NOT NULL,
	[latitude] [float] NOT NULL,
	[longitude] [float] NOT NULL,
	[private] [bit] NOT NULL,
	[price] [smallint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Operators]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Operators](
	[ID] [char](9) NOT NULL,
	[firstName] [nvarchar](20) NOT NULL,
	[surName] [nvarchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Opinions]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Opinions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[talk] [nvarchar](max) NOT NULL,
	[grade] [tinyint] NOT NULL,
	[kindergardenNum] [int] NULL,
	[activityID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Persons]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Persons](
	[ID] [char](9) NOT NULL,
	[firstName] [nvarchar](20) NOT NULL,
	[surName] [nvarchar](20) NOT NULL,
	[dateOfBirth] datetime NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Shifts]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shifts](
	[number] [tinyint] IDENTITY(1,1) NOT NULL,
	[dayInWeek] [tinyint] NULL,
	[shiftType] [char](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SignedFor]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SignedFor](
	[kidID] [char](9) NOT NULL,
	[kindergardenID] [int] NOT NULL,
	[classNumber] [int] NOT NULL,
	[activityID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[kidID] ASC,
	[kindergardenID] ASC,
	[classNumber] ASC,
	[activityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TakesPlace]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TakesPlace](
	[kindergardenID] [int] NOT NULL,
	[classNumber] [int] NOT NULL,
	[activityID] [int] NOT NULL,
	[operatorID] [char](9) NOT NULL,
	[dayInWeek] [tinyint] NOT NULL,
	[startTime] datetime NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[kindergardenID] ASC,
	[classNumber] ASC,
	[activityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Teachers]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Teachers](
	[teacherID] [char](9) NOT NULL,
	[address] [nvarchar](45) NULL,
PRIMARY KEY CLUSTERED 
(
	[teacherID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Training]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Training](
	[ID] [int] NOT NULL,
	[name] [nvarchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TrainingForAssistant]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrainingForAssistant](
	[assistantID] [char](9) NOT NULL,
	[trainingID] [int] NOT NULL,
	[trainingDate] datetime NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[assistantID] ASC,
	[trainingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TrainingParameters]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrainingParameters](
	[number] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](20) NOT NULL,
	[minimumTrainings] [tinyint] NOT NULL,
	[maximumTraining] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[AssistantJoinsActivity]  WITH CHECK ADD FOREIGN KEY([kindergardenID], [classNumber], [activityID])
REFERENCES [dbo].[TakesPlace] ([kindergardenID], [classNumber], [activityID])
GO
ALTER TABLE [dbo].[AssistantJoinsActivity]  WITH CHECK ADD FOREIGN KEY([assistantID], [trainingID])
REFERENCES [dbo].[TrainingForAssistant] ([assistantID], [trainingID])
GO
ALTER TABLE [dbo].[Assistants]  WITH CHECK ADD FOREIGN KEY([assistantID])
REFERENCES [dbo].[Persons] ([ID])
GO
ALTER TABLE [dbo].[AssistIn]  WITH CHECK ADD FOREIGN KEY([kindergardenID], [classNumber])
REFERENCES [dbo].[Classes] ([kindergardenID], [number])
GO
ALTER TABLE [dbo].[AssistIn]  WITH CHECK ADD FOREIGN KEY([assistantID])
REFERENCES [dbo].[Assistants] ([assistantID])
GO
ALTER TABLE [dbo].[AssistIn]  WITH CHECK ADD FOREIGN KEY([shiftNum])
REFERENCES [dbo].[Shifts] ([number])
GO
ALTER TABLE [dbo].[Classes]  WITH CHECK ADD FOREIGN KEY([kindergardenID])
REFERENCES [dbo].[Kindergardens] ([ID])
GO
ALTER TABLE [dbo].[Classes]  WITH CHECK ADD FOREIGN KEY([teacher])
REFERENCES [dbo].[Teachers] ([teacherID])
GO
ALTER TABLE [dbo].[Kids]  WITH CHECK ADD FOREIGN KEY([kindergardenID], [classNumber])
REFERENCES [dbo].[Classes] ([kindergardenID], [number])
GO
ALTER TABLE [dbo].[Kids]  WITH CHECK ADD FOREIGN KEY([kidID])
REFERENCES [dbo].[Persons] ([ID])
GO
ALTER TABLE [dbo].[Opinions]  WITH CHECK ADD FOREIGN KEY([activityID])
REFERENCES [dbo].[Activities] ([ID])
GO
ALTER TABLE [dbo].[Opinions]  WITH CHECK ADD FOREIGN KEY([kindergardenNum])
REFERENCES [dbo].[Kindergardens] ([ID])
GO
ALTER TABLE [dbo].[SignedFor]  WITH CHECK ADD FOREIGN KEY([kindergardenID], [classNumber], [activityID])
REFERENCES [dbo].[TakesPlace] ([kindergardenID], [classNumber], [activityID])
GO
ALTER TABLE [dbo].[SignedFor]  WITH CHECK ADD FOREIGN KEY([kidID])
REFERENCES [dbo].[Kids] ([kidID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TakesPlace]  WITH CHECK ADD FOREIGN KEY([activityID])
REFERENCES [dbo].[Activities] ([ID])
GO
ALTER TABLE [dbo].[TakesPlace]  WITH CHECK ADD FOREIGN KEY([operatorID])
REFERENCES [dbo].[Operators] ([ID])
GO
ALTER TABLE [dbo].[TakesPlace]  WITH CHECK ADD FOREIGN KEY([kindergardenID], [classNumber])
REFERENCES [dbo].[Classes] ([kindergardenID], [number])
GO
ALTER TABLE [dbo].[Teachers]  WITH CHECK ADD FOREIGN KEY([teacherID])
REFERENCES [dbo].[Persons] ([ID])
GO
ALTER TABLE [dbo].[TrainingForAssistant]  WITH CHECK ADD FOREIGN KEY([assistantID])
REFERENCES [dbo].[Assistants] ([assistantID])
GO
ALTER TABLE [dbo].[TrainingForAssistant]  WITH CHECK ADD FOREIGN KEY([trainingID])
REFERENCES [dbo].[Training] ([ID])
GO
ALTER TABLE [dbo].[Activities]  WITH CHECK ADD CHECK  (([length]>=(30) AND [length]<=(90)))
GO
ALTER TABLE [dbo].[Activities]  WITH CHECK ADD CHECK  (([cost]>(0)))
GO
ALTER TABLE [dbo].[Classes]  WITH CHECK ADD CHECK  (([minimumAge]<=[maximumAge]))
GO
ALTER TABLE [dbo].[Classes]  WITH CHECK ADD CHECK  (([maximumAge]<=(7)))
GO
ALTER TABLE [dbo].[Classes]  WITH CHECK ADD CHECK  (([minimumAge]<=(7)))
GO
ALTER TABLE [dbo].[Classes]  WITH CHECK ADD CHECK  (([number]>(0)))
GO
ALTER TABLE [dbo].[Kindergardens]  WITH CHECK ADD CHECK  (([price]>(0)))
GO
ALTER TABLE [dbo].[Operators]  WITH CHECK ADD CHECK  (([ID] like replicate('[0-9]',(9))))
GO
ALTER TABLE [dbo].[Opinions]  WITH CHECK ADD CHECK  (([kindergardenNum] IS NULL AND [activityID] IS NOT NULL OR [kindergardenNum] IS NOT NULL AND [activityID] IS NULL))
GO
ALTER TABLE [dbo].[Opinions]  WITH CHECK ADD CHECK  (([grade]>=(0) AND [grade]<=(10)))
GO
ALTER TABLE [dbo].[Persons]  WITH CHECK ADD CHECK  (([ID] like replicate('[0-9]',(9))))
GO
ALTER TABLE [dbo].[Shifts]  WITH CHECK ADD CHECK  (([dayInWeek]>=(1) AND [dayInWeek]<=(6)))
GO
ALTER TABLE [dbo].[Shifts]  WITH CHECK ADD CHECK  (([shiftType]='N' OR [shiftType]='M'))
GO
ALTER TABLE [dbo].[TakesPlace]  WITH CHECK ADD CHECK  (([dayInWeek]>=(1) AND [dayInWeek]<=(6)))
GO
ALTER TABLE [dbo].[TrainingParameters]  WITH CHECK ADD CHECK  (([maximumTraining]>[minimumTrainings]))
GO
/****** Object:  Trigger [dbo].[checkAssistantInActivity]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE trigger [dbo].[checkAssistantInActivity] on [dbo].[AssistantJoinsActivity] after insert as
declare @kgID int, @clID int, @assiID char(9), @actID int
select @kgID = i.kindergardenID, @clID = i.classNumber, @assiID = i.assistantID, @actID = i.activityID
from inserted as i
--check if there not enough assistants
declare @isEnought bit
set @isEnought = 0
if  (select count(*) from AssistantJoinsActivity aja where aja.kindergardenID = @kgID and aja.classNumber = @clID and aja.activityID = @actID)
	<=
	(select ceiling(count(*)/10.0) as int from SignedFor sf where sf.kindergardenID = @kgID and sf.classNumber = @clID and sf.activityID = @actID)
	begin
		set @isEnought = 1
	end
--check if she have specific training
declare @trainID int = null, @trainName nvarchar(20) = null
select @trainName = t.name
from Training t join Activities a on t.name = a.name
where a.ID = @actID

select @trainID = tfa.trainingID
from TrainingForAssistant tfa join Training t on tfa.trainingID = t.ID
where tfa.assistantID = @assiID and t.name = @trainName
--check if there assistant without specific training
declare @assistantWithoutTraining char(9) = null
select top 1 @assistantWithoutTraining = aja.assistantID
from AssistantJoinsActivity aja
where aja.kindergardenID = @kgID and aja.classNumber = @clID and aja.activityID = @actID and
		not exists (select tfa.trainingID
					from TrainingForAssistant tfa
					where tfa.assistantID = aja.assistantID and tfa.trainingID = @trainID)
--if there is enough assistants, our assistant have training and there is assistant without training
if 0 = @isEnought and @trainID is not null and @assistantWithoutTraining is not null
	begin
		delete from AssistantJoinsActivity where kindergardenID = @kgID and classNumber = @clID and activityID = @actID and assistantID = @assistantWithoutTraining
	end
--check if our assistant have emergency training
declare @isEmergency bit
set @isEmergency = 0
if N'חירום' in (select t.name
				 from TrainingForAssistant tfa join Training t on tfa.trainingID = t.ID
				 where tfa.assistantID = @assiID)
				 begin
					set @isEmergency = 1
				 end
--check if there assistant without emergency training
declare @assistantWithoutEmergency char(9) = null
select top 1 @assistantWithoutEmergency = aja.assistantID
from AssistantJoinsActivity aja
where aja.kindergardenID = @kgID and aja.classNumber = @clID and aja.activityID = @actID and
		N'חירום' not in (select t.name
						 from TrainingForAssistant tfa join Training t on tfa.trainingID = t.ID
						 where tfa.assistantID = aja.assistantID)
--if there is enough assistants, our assistant have emergency training and there assistant without emergency training
if 0 = @isEnought and @trainID is not null and @assistantWithoutTraining is null and @assistantWithoutEmergency is not null
	begin
		delete from AssistantJoinsActivity where kindergardenID = @kgID and classNumber = @clID and activityID = @actID and assistantID = @assistantWithoutEmergency
	end
if 0 = @isEnought and @trainID is null
	begin
		rollback
	end
GO
ALTER TABLE [dbo].[AssistantJoinsActivity] ENABLE TRIGGER [checkAssistantInActivity]
GO
/****** Object:  Trigger [dbo].[kidToClassCheck]    Script Date: 7/14/2017 17:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[kidToClassCheck] ON [dbo].[Kids] AFTER INSERT AS

declare @dateSem varchar(10)
	set @dateSem = CAST(year(getDate()) AS VARCHAR(4))+'-09-01'

DECLARE @kidID char(9)
	SELECT @kidID = INSERTED.kidID
	FROM INSERTED
declare
	@kinderNum int
	select @kinderNum = inserted.kindergardenID
	FROM INSERTED
declare
	@classNum int
	select @classNum = inserted.classNumber
	FROM INSERTED

IF NOT EXISTS (SELECT kidID
			   FROM Kids AS K INNER JOIN 
  				    Persons AS P ON K.kidID = P.ID INNER JOIN 
 				    Classes AS C ON (C.kindergardenID = K.kindergardenID AND 
								     C.number = K.classNumber)
			   WHERE K.kidID = @kidID AND
				     DATEDIFF(YEAR, P.DateOfBirth,@dateSem) BETWEEN C.minimumAge AND 
																	  C.maximumAge)
			   OR
			   	
			   ((SELECT COUNT(kidID)
				FROM Kids k
				WHERE k.kindergardenID = @kinderNum and k.classNumber = @classNum
				GROUP BY k.kindergardenID,k.classNumber)
				>
				(select c.maximumKids
				from Classes c
				where c.kindergardenID = @kinderNum and c.number = @classNum))

			ROLLBACK;
			

--drop trigger kidToClassCheck


GO
ALTER TABLE [dbo].[Kids] ENABLE TRIGGER [kidToClassCheck]
GO
