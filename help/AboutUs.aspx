﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AboutUs.aspx.cs" Inherits="help_AboutUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head_include" Runat="Server">
         
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    About Us
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="headerFirst" Runat="Server">
    Help
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="headerSecond" Runat="Server">
    About iGarden+
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-4">
                <div class="contact-box">
                    <a href="#">
                    <div class="col-sm-4">
                        <div class="text-center">
                            <img alt="image" style="width: 100px; height: 100px;"  class="img-circle m-t-xs img-responsive" src="/images/mickey.jpg">
                            <div class="m-t-xs font-bold">Full Stack Developer</div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <h3><strong>Mickey Shalev</strong></h3>
                        <p><i class="fa fa-map-marker"></i> Kiryat Haim</p>
                        <address>
                            <strong>iGarden+ Inc.</strong><br>
                            Grinboim 58,<br>
                            Haifa<br>
                            <abbr title="Phone">P:</abbr> (72) 54-7205195
                        </address>
                    </div>
                    <div class="clearfix"></div>
                        </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="contact-box">
                    <a href="#">
                    <div class="col-sm-4">
                        <div class="text-center">
                            <img alt="image" style="width: 100px; height: 100px;" class="img-circle m-t-xs img-responsive" src="/images/nisan.jpg">
                            <div class="m-t-xs font-bold">Full Stack Developer</div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <h3><strong>Nisan Bahar</strong></h3>
                        <p><i class="fa fa-map-marker"></i> Haifa</p>
                        <address>
                            <strong>iGarden+ Inc.</strong><br>
                            Grinboim 58,<br>
                            Haifa<br>
                            <abbr title="Phone">P:</abbr> (72) 50-8557689
                        </address>
                    </div>
                    <div class="clearfix"></div>
                        </a>
                </div>
            </div>
    
                </div>
            </div>
     
</asp:Content>

<asp:Content ContentPlaceHolderID="jsLoad" runat="server">
      <!-- Mainly scripts -->
    <script src="/js/jquery-2.1.1.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>



    <!-- Chosen -->
    <script src="/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="/js/plugins/jsKnob/jquery.knob.js"></script>

   

    <!-- IonRangeSlider -->
    <script src="/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>
</asp:Content>