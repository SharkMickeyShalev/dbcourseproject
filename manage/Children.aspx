﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Children.aspx.cs" Inherits="manage_Children" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    Children Management
</asp:Content>
<asp:Content ContentPlaceHolderID="headerFirst" runat="server">
    Manage
</asp:Content>
<asp:Content ContentPlaceHolderID="headerSecond" runat="server">
    Children
</asp:Content>

<asp:Content ContentPlaceHolderID="head_include" runat="server">
      
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/font-awesome/css/font-awesome.css" rel="stylesheet">
      <link href="/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
        <div class="wrapper wrapper-content animated fadeInRight">
     
        <div runat="server" id="kidsContent"
           class="row">
       

         
        </div>
        
            
        </div>


</asp:Content>


<asp:Content ContentPlaceHolderID="jsLoad" runat="server">
     <!-- Mainly scripts -->
    <script src="/js/jquery-2.1.1.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>         <!-- Sweet alert -->
    <script src="/js/plugins/sweetalert/sweetalert.min.js"></script>

   

</asp:Content>
