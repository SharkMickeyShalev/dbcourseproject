﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Collections.Specialized;

public partial class manage_Children : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


        //Check if kid deleted
        Uri unparsedUrl = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);


        string query = unparsedUrl.Query;

        var queryParams = HttpUtility.ParseQueryString(query);
        string delid = queryParams["delid"];
        
        
        Manager.log("\n\n\nURI: " + delid + "\n\n\n");

        if (!string.IsNullOrEmpty(delid))
        {
            Manager.log("FORM");
            Dictionary<String, Object> toDelete = new Dictionary<string, object>();
            toDelete.Add("kidID", delid);
            Manager.runProcWithResults("delete_kid", toDelete);
            Manager.addNotification("Successfully deleted", "You have successfully removed your child from iGarden System", 0);

        }

        //Get parents' kids details
        Dictionary<String, Object> toSendMap = new Dictionary<string, object>();
        if (queryParams["ParentID"]!=null)
            toSendMap.Add("id", queryParams["ParentID"]);
        else
            toSendMap.Add("id", ((User)Session["user"]).getUserID());

            DataTable kidsTable = Manager.runProcWithResults("get_my_kids", toSendMap);
     
            if (kidsTable.Rows.Count == 0)
            {
            Manager.log("Manage Children -> No kids found!");
            //Parent has no kids

        }else
        {
            Manager.log("Found " + kidsTable.Rows.Count + " kids");
            kidsContent.InnerHtml = getKidsBlocks(kidsTable);
        }
    


    }
    
    protected int getAge(DateTime date)
    {
        DateTime today = DateTime.Today;
        int age = today.Year - date.Year;
        if (date > today.AddYears(-age)) age--;
        return age;
    }

    protected String getKidsBlocks(DataTable kidsList)
    {
        String html = "";
        foreach (DataRow kidRow in kidsList.Rows)
        {

            html += @"  <div class='col-lg-4'>
                <div class='contact-box'>
                   
                    <div class='col-sm-4'>
                        <div class='text-center'>
                            <img alt='image' class='img-circle m-t-xs img-responsive' src='/images/avatars/child.png'><br />
<i class='fa fa-credit-card'>&nbsp;&nbsp;</i>"+kidRow["kidID"]+@"
                            <div class='m-t-xs font-bold'>
                                <asp:Literal ID='lblKinderGarden' runat='server'></asp:Literal>
                            </div>
                        </div>
                    </div>
<script>
function doConfirm() {
    swal({
  title: 'Are you sure?',
  text: 'You will not be able to recover your child data!',
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#DD6B55',
  confirmButtonText: 'Yes, delete it!',
  closeOnConfirm: false
},
function(){
var delayMillis = 2000;
swal ('Kid Deleted!','" + kidRow["firstName"] + @" has been deleted from iGarden+ Children Management','success');
setTimeout(function() {
  //your code to be executed after 1 second

window.location = '/manage/Children.aspx?delid=" + kidRow["kidID"] + @"'
}, delayMillis);
  
});
}
        </script>
                    <div class='col-sm-8'>
<a onclick='doConfirm()'
><i class='fa fa-times'></i></a>
                        <h3><strong>
                            " + kidRow["firstName"] + " " + kidRow["surName"] + @"
                            </strong></h3>
                        <p><i class='fa fa-map-marker'></i> 
    " + kidRow["street"] + " " + kidRow["houseNumber"] + ", " + kidRow["city"] + @" 
</p>
<p>
    <strong>Age: </strong> " + getAge(Convert.ToDateTime(kidRow["dateOfBirth"])) + @"
</p>
                        <address>
                            <strong>Kindergarden: " + kidRow["name1"] + @"</strong><br>
                            Teacher: " + kidRow["surName1"] + " " + kidRow["firstName1"] + @"<br>
                            " + kidRow["street1"] + " " + kidRow["houseNumber1"] + ", " + kidRow["city1"] + @"<br><br />
                           ";
            Dictionary<String, Object> tt = new Dictionary<string, object>();
           tt.Add("kidid", kidRow["kidID"]);
            
            foreach (DataRow Stats in Manager.runProcWithResults("get_kid_details", tt).Rows){
                html += @"
                <i class='fa fa-gamepad'>&nbsp;&nbsp;</i>Total Activities: " + Stats["TotalActivities"] + @"<br />
                <i class='fa fa-dollar'>&nbsp;&nbsp;</i>Kindergarden Cost: $" + Stats["kinderPrice"] + @"/year<br />
                <i class='fa fa-dollar'>&nbsp;&nbsp;</i>Activities Cost: $" + Stats["activityCosts"] + @"/year<br />
                <i class='fa fa-dollar'>&nbsp;&nbsp;</i>Total Cost: $" + Stats["TotalPrice"] + @"/year<br />
";
            }


            html+=@"
                        </address>

<br /><br />
 <a href='/schedule/?kinder=" + kidRow["kindergardenID"] + @"&class="+kidRow["number"]+"&kid="+kidRow["kidID"]+@"' class='btn btn-xs btn-outline btn-primary'>View Class Schedule <i class='fa fa-long-arrow-right'></i> </a>
                    </div>
                    <div class='clearfix'></div>
                       
                </div>
            </div>";

        }
        return html;
    }


}


