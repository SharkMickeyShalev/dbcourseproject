﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="opinions_Default" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head_include" Runat="Server">
      <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    View Opinions
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="headerFirst" Runat="Server">
    Opinions
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="headerSecond" Runat="Server">
    Select
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content text-center p-md">

                    <h2><span class="text-navy">iGarden+ Opinions Manager</span>
                    allows you to view our reviews from our beloves customers!</h2>

                    <p>
                        Please select if you wish to view our Kindergardens or Activities reviews
                    </p>


                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-content text-center p-md">

                    <h4 class="m-b-xxs">Kindergardens Reviews</h4><br />
                    <small>(Total Reviews: <asp:Literal ID="totalKGReviews" text="0" runat="server"></asp:Literal>)</small>
            <br /><br />
                    <a href="/opinions/kindergardens.aspx">
                    <span class="simple_tag">View Kindergardens Opinions</span>
                        </a>
       
                    <div class="m-t-md">
                        <div class="p-lg ">
       
                            <img style="width: 100%; height: 50%;" class="img-responsive img-shadow" src="/images/kindergardens.jpg" alt="">

                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-content text-center p-md">

                    <h4 class="m-b-xxs">Activities Reviews</h4><br />
                  <small>(Total revious: <asp:Literal ID="totalReviews" text="0" runat="server"></asp:Literal>)</small><br /><br />
                        <a href="/opinions/activities.aspx">
                    <span class="simple_tag">View Activities Reviews</span>
                        </a>
                    <div class="m-t-md">
                        <div class="p-lg">
                     <img class="img-responsive img-shadow" style="width: 100%; height: 50%;" src="/images/activity.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
   


</div>

</asp:Content>

<asp:Content ContentPlaceHolderID="jsLoad" runat="server">
      <!-- Mainly scripts -->
        <!-- Mainly scripts -->
    <script src="/js/jquery-2.1.1.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>



</asp:Content>

