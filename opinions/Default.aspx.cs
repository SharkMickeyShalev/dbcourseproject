﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Data;

public partial class opinions_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable dt = Manager.runProcWithResults("get_reviews_statistics");
        totalKGReviews.Text = dt.Rows[0][0].ToString();
        totalReviews.Text = dt.Rows[0][1].ToString();
    }
}