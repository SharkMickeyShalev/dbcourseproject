﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Data;


public partial class opinions_activities : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        activities.Text = loadActivities();

    }

    protected string loadActivities()
    {
        string toReturn = "";

        DataTable dt = Manager.runProcWithResults("get_opinions_act_list");
        foreach (DataRow row in dt.Rows)
        {
            if (row["avg"].ToString().Length == 0)
                row["avg"] = "0";
            float parsedRank = float.Parse(row["avg"].ToString());
            string color = Manager.getColorByFloat(parsedRank);
            toReturn +=
           @"  <div class='col-md-3'>
                    <div class='ibox'>
                        <div class='ibox-content product-box'>

                            <div class='product-imitation'>
                                <span style='color: gray; font-size: 18px;'>" + row["name"] + @"</span>
                               
                                <img style='width: 100%; height: 250px;' src='/images/activities/" + row["id"] + @".jpg' />
                            </div>
                            <div class='product-desc'>
                                <span style='background-color: " + color + @";' class='product-price'>
                                    Score: " + row["avg"] + @"/10
                                </span>

                                <a href='#' class='product-name'>" + row["name"] + @"</a>



                                <div class='small m-t-xs'>
                                    Cost: $" + row["cost"] + @"<br />
                                    Length: " + row["length"] + @" minutes<br />
                                    Occured in: " + row["diffKG"] + @" Kindergardens<br />

                                </div>
                                <div class='m-t text-righ'>
";
            if (row["totalReviews"].ToString().Length > 0)
                toReturn += @"
                                    <a href='/opinions/view.aspx?act=" + row["id"] + @"' class='btn btn-xs btn-outline btn-primary'>View " + row["totalReviews"] + @" Reviews <i class='fa fa-long-arrow-right'></i> </a>";
            toReturn +=@"
<a href='/opinions/opinion.aspx?activity=" + row["id"] + @"' class='btn btn-xs btn-outline btn-primary' style='color: gray; border-color: gray;'>Post a Review<i class='fa fa-long-arrow-right'></i> </a>
";
            toReturn+=@"
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

";
        }


        return toReturn;
    }
}