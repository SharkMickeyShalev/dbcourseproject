﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Data;

public partial class opinions_classes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        Uri unparsedUrl = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
        string query = unparsedUrl.Query;
        var queryParams = HttpUtility.ParseQueryString(query);

        int kindergardenID = -1;

        try
        {
            kindergardenID = int.Parse(queryParams["kinder"]);
            classes.Text = loadClasses(kindergardenID);
        }
        catch (Exception ex)
        {
            Manager.log("Caught exception: " + ex);
        }
        
    }

    protected string loadClasses(int kindergardenID)
    {
        Dictionary<String, Object> toSend = new Dictionary<string, object>();
        toSend.Add("kindergardenID", kindergardenID);
        DataTable dt = Manager.runProcWithResults("get_classes_by_kindergarden", toSend);
        String toReturn = "";
        foreach (DataRow row in dt.Rows)

        {
            int ClassNum = int.Parse(row["number"].ToString());
            String className = row["name"].ToString();
            int minAge = int.Parse(row["minimumAge"].ToString());
            int maxAge = int.Parse(row["maximumAge"].ToString());
            int maxKids = int.Parse(row["maximumKids"].ToString());
            String teacherName = row["firstName"].ToString() + " " + row["surName"].ToString();
            int registeredKids = int.Parse(row["registeredKids"].ToString());
            int numActivities = int.Parse(row["activities"].ToString());
            String kinderName = row["name1"].ToString();
             String kinderType = ((bool)row["private"]) ? "Private" : "Public";


            toReturn +=
           @"  <div class='col-md-3'>
                    <div class='ibox'>
                        <div class='ibox-content product-box'>

                            <div class='product-imitation'>
                                <span style='color: gray; font-size: 18px;'>" + kinderName + " - " + className + @"</span>
                                <br /><br />
                                <img style='width: 100%; height: 100%;' src='/images/classes.jpg' />
                            </div>
                            <div class='product-desc'>
                                <span class='product-price' style='background-color: gray;'>
                                    # Kids: " + registeredKids.ToString() + @"/" + maxKids + @" 
                                </span>
                                <small class='text-muted'>" + kinderType + @" Kindergarden</small>
                                <a href='/schedule/?kinder=" + kindergardenID + "&class=" + ClassNum + "' class='product-name'>" + className + @"</a>

                                <div>
                                    <p><i class='fa fa-female'></i> Teacher: " + teacherName + @"</p>
                                </div>

                                <div class='small m-t-xs'>
                                    <p><i class='fa fa-map-marker'></i> Age Range: " + minAge + " - " + maxAge + @"</p>
                                </div>
<div class='small m-t-xs'>
                                    <p><i class='fa fa-gamepad'></i> # Activities: " + numActivities + @"</p>
                                </div>
                                <div class='m-t text-righ'>

<br /><br />
                                        <a href='/schedule/?kinder=" + kindergardenID +"&class="+ClassNum+ @"'  style='color: gray; border-color: gray;' class='btn btn-xs btn-outline btn-primary'>View Class Schedule<i class='fa fa-long-arrow-right'></i> </a>
<br />
";
            if (Manager.loggedUser.getAuth().Equals(EAuth.Administrator))
                toReturn += @"<a href='/add/activity.aspx?kinder=" + kindergardenID + "&class="+ ClassNum +@"'  style='color: green; border-color: green;' class='btn btn-xs btn-outline btn-primary'>Add Activity<i class='fa fa-long-arrow-right'></i> </a>";
            toReturn +=@"
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

";
        }
    


        return toReturn;
    }
}