﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Data;

public partial class opinions_kindergardens : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        kindergardens.Text = loadKindergardens();
       
        
    }

    protected string loadKindergardens()
    {
        string toReturn = "";

        DataTable dt = Manager.runProcWithResults("get_opinions_kg_list");
        foreach (DataRow row in dt.Rows)
        {
            if (row["avg"].ToString().Length == 0)
                row["avg"] = "0";
            float parsedRank = float.Parse(row["avg"].ToString());
            string color = Manager.getColorByFloat(parsedRank);
            toReturn +=
           @"  <div class='col-md-3'>
                    <div class='ibox'>
                        <div class='ibox-content product-box'>

                            <div class='product-imitation'>
                                <span style='color: gray; font-size: 18px;'>"+row["name"]+@"</span>
                                <br /><br />
                                <img style='width: 100%; height: 100%;' src='/images/kindergardens.jpg' />
                            </div>
                            <div class='product-desc'>
                                <span class='product-price' style='background-color: "+color+@";'>
                                    Score: " + row["avg"] + @"/10
                                </span>
                                <small class='text-muted'>"+row["KG"]+ @" Kindergarden</small>
                                    <a href='/opinions/classes.aspx?kinder=" + row["id"] + @"' class='product-name'>" + row["name"] + @"</a>



                                <div class='small m-t-xs'>
                                    <p><i class='fa fa-map-marker'></i> " + row["street"] + " " + row["houseNumber"] + ", " + row["city"] + @"</p>
                                </div>
                                <div class='m-t text-righ'>
";
            if (row["totalReviews"].ToString().Length > 0)
                toReturn += @"
                                    <a href='/opinions/view.aspx?kinder=" + row["id"] + @"' class='btn btn-xs btn-outline btn-primary'>View " + row["totalReviews"] + @" Reviews <i class='fa fa-long-arrow-right'></i> </a>
<a href='/opinions/opinion.aspx?kinder=" + row["id"] + @"' class='btn btn-xs btn-outline btn-primary' style='color: gray; border-color: gray;'>Post a Review<i class='fa fa-long-arrow-right'></i> </a>
";
            toReturn += @"
<br /><br />
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

";
        }


        return toReturn;
    }
}