﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Data;
using System.Drawing;
using System.Collections.Specialized;

public partial class add_Child : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        
        Uri unparsedUrl = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
        string query = unparsedUrl.Query;
        var queryParams = HttpUtility.ParseQueryString(query);

        int kindergarden = -1;
        int activity = -1;

        try
        {
            activity = int.Parse(queryParams["activity"]);
        }
        catch (Exception) {
            try
            {
                kindergarden = int.Parse(queryParams["kinder"]);
            }
            catch (Exception ex)
            {
                
            }
        }
        
       


        if (kindergarden>0)
        {
            opinion_type.Text = getKindergardenName(kindergarden);
            //get kindergarden details
          
        }else if (activity>0)
        {
            opinion_type.Text = getActivityName(activity);
            //get activity details

        }
        else
        {
            opinion_type.Text = "Error";
            return;
        }

        Manager.log(activity + "");

        if (IsPostBack)
        {
            String text = opinion_text.Text;
            NameValueCollection nvc = Request.Form;
            int grade = int.Parse(nvc["grade"]);

            if (grade > 0 && grade <= 10 && text.Length > 1)
            {


                //add kindergarden opinion
                if (kindergarden > 0)
                {
                    addKinderOpinion(text, grade, kindergarden);

                }
                //add activity opinion
                else if (activity > 0)
                {

                    addActivityOpinion(text, grade, activity);

                }

            }
            else
            {
                Manager.addNotification("Couldn't add opinion", "We are sorry,<br />But you must add grade and text to your opinion", 1);
                Manager.log("FATAL ERROR trying to add opinion without text or grade");
            }

        }
     
    }





    private String getKindergardenName(int id)
    {
        String kgName = null;
        Dictionary<String, Object> getkg = new Dictionary<string, object>();
        getkg.Add("id", id);

        DataTable dt = Manager.runProcWithResults("get_kindergarden_details", getkg);

        if (dt.Rows.Count > 0)
        {
            kgName = dt.Rows[0]["name"].ToString();
        }


        return kgName;
    }



    private String getActivityName(int id)
    {
        String acName = null;
        Dictionary<String, Object> getAc = new Dictionary<string, object>();
        getAc.Add("ID", id);

        DataTable dt = Manager.runProcWithResults("get_activity_name", getAc);

        if (dt.Rows.Count > 0)
        {
            acName = dt.Rows[0]["name"].ToString();
        }


        return acName;
    }




    private void addKinderOpinion(String text, int grade, int kindergarden)
    {
        Dictionary<String, Object> addOpinion = new Dictionary<string, object>();
        addOpinion.Add("talk", text);
        addOpinion.Add("grade", grade);
        addOpinion.Add("kinder", kindergarden);
        addOpinion.Add("creator", Manager.loggedUser.getUserID());

        DataTable dt = Manager.runProcWithResults("add_kindergarden_opinion", addOpinion);
        if (dt.Rows.Count > 0)
        {
            Manager.addNotification("Added a Opinion", "Successfully added opinion for kindergarden.<br />" + text , 0);
            Manager.log("Successfully added opinion to kindergarden<br />" + text);
            Response.Redirect("/opinions/kindergardens.aspx");
        }
        else
        {
            Manager.addNotification("Couldn't add opinion", "We are sorry,<br />But it seems like there is a problem.", 1);
            Manager.log("FATAL ERROR on trying to add kindergarden opinion");
        }

    }



    private void addActivityOpinion (String text, int grade, int activity)
    {
        Dictionary<String, Object> addOpinion = new Dictionary<string, object>();
        addOpinion.Add("talk", text);
        addOpinion.Add("grade", grade);
        addOpinion.Add("activity", activity);
        addOpinion.Add("creator", Manager.loggedUser.getUserID());

        DataTable dt = Manager.runProcWithResults("add_activity_opinion", addOpinion);
        if (dt.Rows.Count > 0)
        {
            Manager.addNotification("Added a Opinion", "Successfully added opinion for activity.<br /> " + text, 0);
            Manager.log("Successfully added opinion to activity:<br /> " + text);
            Response.Redirect("/opinions/activities.aspx");
        }
        else
        {
            Manager.addNotification("Couldn't add opinion", "We are sorry,<br />But it seems like there is a problem.", 1);
            Manager.log("FATAL ERROR on trying to add activity opinion");
        }
    }


}
 