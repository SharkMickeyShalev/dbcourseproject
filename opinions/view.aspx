﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="view.aspx.cs" Inherits="opinions_view" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head_include" Runat="Server">
             
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/plugins/footable/footable.core.css" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    View Opinions
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="headerFirst" Runat="Server">
    Opinions
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="headerSecond" Runat="Server">
    View Opinions
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>FooTable with row toggler, sorting and pagination</h5>

                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">

                            <table class="footable table table-stripped toggle-arrow-tiny footable-loaded tablet breakpoint" data-page-size="8">
                                <thead>
                                <tr>

                                    <th data-toggle="true" class="footable-visible footable-sortable footable-first-column">Opinion<span class="footable-sort-indicator"></span></th>
                                    <th class="footable-visible footable-sortable">Grade<span class="footable-sort-indicator"></span></th>
                                   
                                    <th class="footable-visible footable-sortable footable-last-column">Action<span class="footable-sort-indicator"></span></th>
                                </tr>
                                </thead>
                                <tbody>
                                
                                
                                
                                
                                    <asp:Literal ID="ltOpinions" runat="server"></asp:Literal>  
                                
                                
                                
                                
                                
                                
                                
                                
                                
                             </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5" class="footable-visible">
                                        <ul class="pagination pull-right"><li class="footable-page-arrow disabled"><a data-page="first" href="#first">«</a></li><li class="footable-page-arrow disabled"><a data-page="prev" href="#prev">‹</a></li><li class="footable-page active"><a data-page="0" href="#">1</a></li><li class="footable-page"><a data-page="1" href="#">2</a></li><li class="footable-page-arrow"><a data-page="next" href="#next">›</a></li><li class="footable-page-arrow"><a data-page="last" href="#last">»</a></li></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            
                </div>
        
      <!-- FooTable -->
  
</asp:Content>

  <asp:Content ID="Content6" ContentPlaceHolderID="jsLoad" Runat="Server">
 
        <!-- Mainly scripts -->
    <script src="/js/jquery-2.1.1.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>



    <!-- Chosen -->
    <script src="/js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="/js/plugins/jsKnob/jquery.knob.js"></script>

   

    <!-- IonRangeSlider -->
    <script src="/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

  
    <script src="/js/plugins/footable/footable.all.min.js"></script>    
 
       <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function() {

            $('.footable').footable();
            $('.footable2').footable();

        });

    </script>        </asp:Content>