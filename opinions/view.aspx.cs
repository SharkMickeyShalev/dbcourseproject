﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Data;

public partial class opinions_view : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Uri unparsedUrl = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
        string query = unparsedUrl.Query;
        var queryParams = HttpUtility.ParseQueryString(query);


        if (queryParams["delo"] != null)
        {
            //Try deleting an opinion
            Dictionary<String, Object> deloMap = new Dictionary<string, object>();
            deloMap.Add("id", queryParams["delo"]);
            Manager.runProcWithResults("delete_opinion", deloMap);
            Manager.addNotification("Success!", "Opinion #" + queryParams["delo"] + " has been deleted successfully!", 0);
           

            
        }

        int kinderID = 0;
        int activityID = 0;

        try
        {
            kinderID = int.Parse(queryParams["kinder"].ToString());
        }
        catch (Exception)
        {
            try
            {
                activityID = int.Parse(queryParams["act"].ToString());
            }
            catch (Exception)
            {
                return;
            }
        }

        if (activityID > 0)
            ltOpinions.Text=loadOpinions(activityID, false);
        else ltOpinions.Text=loadOpinions(kinderID, true);


    }



    protected String loadOpinions(int id, bool Kinder)
    {
        Dictionary<String, Object> getOpinionsMap = new Dictionary<string, object>();
        getOpinionsMap.Add("id", id);
        DataTable dt = null;
        if (Kinder)
            dt = Manager.runProcWithResults("get_opinions_by_kindergarden", getOpinionsMap);
        else dt = Manager.runProcWithResults("get_opinions_by_activity", getOpinionsMap);
        String toReturn = "";
        String delStr = Kinder == true ? "kinder" : "act";
        foreach (DataRow row in dt.Rows) {
            toReturn += @"  <tr class='footable-even' style='display: table-row;'>
                                    <td class='footable-visible footable-first-column'><span class='footable-toggle'></span>" + row["talk"] + @"</ td >
                                        <td class='footable-visible' style='color: " + Manager.getColorByFloat(float.Parse(row["grade"].ToString())) + @";'>" + row["grade"] + @"/10</td>
                                  
                                    <td class='footable-visible footable-last-column'><a href = '/opinions/view.aspx?" + delStr+"="+id+"&delo="+row["id"]+@"' ><i class='fa fa-times text-navy'></i></a></td>
                                </tr>


";
        }

        return toReturn;
    }
}