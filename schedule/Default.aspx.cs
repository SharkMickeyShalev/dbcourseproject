﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iGarden;
using System.Data;

public partial class schedule_Default : System.Web.UI.Page
{
    public String txt;
    string kidID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        Manager.update();
        Uri unparsedUrl = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
        string query = unparsedUrl.Query;
        var queryParams = HttpUtility.ParseQueryString(query);



        if (Manager.loggedUser.getAuth().Equals(EAuth.Parent))
        {
            txtLegend.Text = @"
             <div class='col-lg-3'>
            <div class='ibox float-e-margins'>
                <div class='ibox-title'>
                    <h5>Activity Legend</h5>
                    <div class='ibox-tools'>
                        <a class='collapse-link'>
                            <i class='fa fa-chevron-up'></i>
                        </a>
                        <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                            <i class='fa fa-wrench'></i>
                        </a>
                        <ul class='dropdown-menu dropdown-user'>
                            <li><a href = '#' > Config option 1</a>
                            </li>
                            <li><a href = '#' > Config option 2</a>
                            </li>
                        </ul>
                        <a class='close-link'>
                            <i class='fa fa-times'></i>
                        </a>
                    </div>
                </div>
                <div class='ibox-content'>
                    <div id = 'external-events' >
                        <div class='external-event navy-bg' style='background-color: #000000;'>Kid is not signed to Activity</div>
                        <div class='external-event navy-bg'>Kid is signed to Activity</div>
  
                      
                    </div>
                </div>
            </div>
           
        </div>";
        }

        int kindergardenID = -1;
        int classNum = -1;

        kindergardenID = int.Parse(queryParams["kinder"]);
        classNum = int.Parse(queryParams["class"]);


        try
        {
            //Sign to activity
            int actID = int.Parse(queryParams["signto"]);
            Dictionary<String, Object> actmap = new Dictionary<string, object>();
            actmap.Add("ID", actID);
            string actName = Manager.runProcWithResults("get_activity_name", actmap).Rows[0][0].ToString();

            string kidID = queryParams["kid"];
            Dictionary<String, Object> toSign = new Dictionary<string, object>();
            toSign.Add("ID", kidID);
            toSign.Add("kindergardenID", kindergardenID);
            toSign.Add("classNumber", classNum);
            toSign.Add("activityID", actID);

            if (int.Parse(Manager.runProcWithResults("sign_kid_to_activity", toSign).Rows[0][0].ToString()) == 0)
            {
                Manager.addNotification("Success!", "Your child is now not registered to activity " + actName, 0);
            }
            else
            {
                Manager.addNotification("Success!", "We have successfully registered your child to the activity " + actName, 0);
            }
            
                

        }
        catch (Exception)
        {

        }


        List<int> kidsActivities = new List<int>();
        try
        {
            kidID = queryParams["kid"];
            Dictionary<String, Object> map = new Dictionary<string, object>();
            map.Add("kidid", kidID);
            DataTable dt = Manager.runProcWithResults("get_kid_activities", map);
            foreach(DataRow row in dt.Rows)
            {
                kidsActivities.Add(int.Parse(row["activityid"].ToString()));
            }
        }
        catch (Exception)
        {

        }



        if (queryParams["act"] != null)
        {


            try
            {


                int activity = int.Parse(queryParams["act"]);
                int newday = int.Parse(queryParams["newday"]);
                int newhh = int.Parse(queryParams["newhh"]);
                int newmm = int.Parse(queryParams["newmm"]);
                DateTime date = new DateTime(2017, 1, 1, newhh, newmm, 0);
                Dictionary<String, Object> toSend = new Dictionary<string, object>();
                toSend.Add("kinderid", kindergardenID);
                toSend.Add("classnum", classNum);
                toSend.Add("activityid", activity);
                toSend.Add("dayofweek", newday);
                toSend.Add("start", date);

                if (Manager.runProcWithResults("edit_activity_of_kindergarden", toSend).Rows.Count <= 0)
                    throw new Exception();

                Manager.addNotification("Event changed", "You have successfully moved the activity.", 0);
            }

            catch (Exception)
            {
                Manager.addNotification("Event Change Failure", "We are sorry, but we could not process your request.", 1);
            }
        }
        this.txt = @"

<script>

    $(document).ready(function() {

            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green'
            });

        /* initialize the external events
         -----------------------------------------------------------------*/


        $('#external-events div.external-event').each(function() {

            // store data so the calendar knows to render an event upon drop
            $(this).data('event', {
                    title: $.trim($(this).text()), // use the element's text as the event title
                stick: true // maintain when user navigates (see docs on the renderEvent method

            });

            // make the event draggable using jQuery UI
            $(this).draggable({
                    zIndex: 1111999,
                revert: true,      // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });

            });


            /* initialize the calendar
             -----------------------------------------------------------------*/
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

        $('#calendar').fullCalendar({
                header:
                {
                    left: 'prev,next today',
                center: 'title',
            
               

            },
";
        if(Manager.loggedUser.getAuth().Equals(EAuth.Parent) && kidID.Length > 0)
        {
            txt += @"
eventClick: function(calEvent, jsEvent, view) {
    
  swal({
  title: 'Register to activity ' + calEvent.title,
  text: 'Please confirm swap of registration to activity ' + calEvent.title,
type: 'warning',

  showCancelButton: true,
  closeOnConfirm: false,
  animation: 'slide-from-top',
  showLoaderOnConfirm: true,

},
function(isConfirm){
var delayMillis = 1500;
  if (isConfirm) {
     swal('Nice!', 'You have successfully swapped registration to activity ' + calEvent.title, 'success');

setTimeout(function() {
  //your code to be executed after 1 second

window.location = '/schedule/?kinder=" + kindergardenID + "&class=" + classNum + "&kid="+kidID+@"&signto='+calEvent.id;

}, delayMillis);
  

  } else {

setTimeout(function() {
  //your code to be executed after 1 second
 swal('Cancelled','You have cancelled the event change','error');
    
}, 100);
}
}



);
},
";
        

       
    }
        txt+=@"
 eventDrop:  function(event, jsEvent, view) {

";
        if (!Manager.loggedUser.getAuth().Equals(EAuth.Administrator))
        {
            txt += @"     swal({
  title: 'No Permissions!',
  text: 'We are sorry, but only system administrators can change event times.\n\nYou can click on an activity in order to sign your kid to it!',
type: 'error',

},
function(){


window.location = '/schedule/?kinder="+kindergardenID+"&class="+classNum+"&kid="+kidID+@"';

});

";


        }
        else {
            txt += @"
     swal({
  title: 'Editing event time: ' + event.title,
  text: 'Will change  event time\n to ' + moment(event.start).format('dddd hh:mm') + ' - ' + moment(event.end).format('hh:mm'),
type: 'warning',

  showCancelButton: true,
  closeOnConfirm: false,
  animation: 'slide-from-top',
  showLoaderOnConfirm: true,

},
function(isConfirm){
var delayMillis = 1500;
  if (isConfirm) {
     swal('Nice!', 'You changed event time to ' + moment(event.start).format('dddd hh:mm') + ' - ' + moment(event.end).format('hh:mm'), 'success');

setTimeout(function() {
  //your code to be executed after 1 second

window.location = '/schedule/?kinder=" + kindergardenID + "&class=" + classNum + @"&act='+event.id+'&newday=' + (moment(event.start).day()+1) + '&newhh='+moment(event.start).hours() + '&newmm='+moment(event.start).minutes()
}, delayMillis);
  

  } else {

setTimeout(function() {
  //your code to be executed after 1 second
 swal('Cancelled','You have cancelled the event change','error');
    
}, 100);
}
}



);
"; }
        txt+= @"
    },

defaultView: 'agendaWeek',
            editable: true,
    eventDurationEditable: false,
minTime: '10:00:00',
maxTime: '23:00:00',
slotDuration: '00:20:00',
    contentHeight: 1200,
            droppable: true, // this allows things to be dropped onto the calendar
            drop: function() {
                    
                    if ($('#drop-remove').is(':checked')) {
                
                    $(this).remove();
                    }
                },

        
            events: [
";
        foreach (DataRow row in getEvents(kindergardenID, classNum).Rows)
        {
            DateTime today = DateTime.Today;
            DateTime dateTime = getDateTime(row["startTime"].ToString());
            DateTime endDateTime = dateTime.AddMinutes(double.Parse(row["length"].ToString()));
            int findDiff = ((int)today.DayOfWeek + 1) - int.Parse(row["dayInWeek"].ToString());
           // Manager.log("Activity: "+row["name"]+" (" + (int)today.DayOfWeek + " + 1) - " + int.Parse(row["dayInWeek"].ToString()));
            findDiff += 1;
            txt += @"
    {
id: '" + row["activityID"] + @"',
        title: '" + row["name"] + @"',
        start: new Date(y, m, d-(" + findDiff + "), " + dateTime.Hour + ", " + dateTime.Minute + @"),
        end: new Date(y, m, d-(" + findDiff + "), " + endDateTime.Hour + ", " + endDateTime.Minute + @"),
        

";
            if (!kidsActivities.Contains(int.Parse(row["activityID"].ToString())))
            {
                txt += "backgroundColor: '#000000'";
            }
txt+=@"
        },";
           
        }
        txt+= @"

            ]
        });


    });

</script>
";

        
    }

    protected DateTime getDateTime(String dt)
{
    DateTime toReturn = Convert.ToDateTime(dt);
    return toReturn;
}

    public DataTable getEvents(int kindergardenID, int classNum)
    {
       

            Dictionary<String, Object> toSend = new Dictionary<string, object>();
        toSend.Add("kindergardenID", kindergardenID);
        toSend.Add("classnum", classNum);
        return Manager.runProcWithResults("get_activities_times", toSend);
        
    }
}